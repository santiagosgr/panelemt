<?php
if (isset($_POST)) {
    require 'lib.php';
 
    $id = $_POST['id'];
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $email = $_POST['email'];
    $fnac = $_POST['fnac'];
    $tel = $_POST['tel'];
    $telseg = $_POST['telseg'];
    $paq = $_POST['paq'];
    $pventa = $_POST['pventa'];
    $pcosto = $_POST['pcosto'];
    $vendedor = $_POST['vendedor'];
    $dif = $pventa - $pcosto;
    $dni = $_POST['dni'];
    $familiar = $_POST['familiar'];
 
    $object = new CRUD();
 
    $object->Update($first_name, $last_name, $email, $fnac, $tel, $telseg, $paq, $pventa, $pcosto, $dif, $dni, $vendedor, $familiar, $id);
}

?>