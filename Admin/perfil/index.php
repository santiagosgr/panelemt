<?php
	session_start();
include 'db.php';
include '../pasajeros/ajax/lib.php';

$object = new CRUD();
$cant_emitidos = $object->EmitidosPorVendedor($_SESSION['Nombre']);


$rentabilidad = $object->RentabilidadVendedor($_SESSION['Nombre']);



if ($cant_emitidos != 0) {
  $rentabilidad_prom = round($rentabilidad / $cant_emitidos, 2); 

} else {
  $rentabilidad_prom = 0;
}





$stmt = $pdo->prepare("SELECT COUNT(*) FROM users");
$stmt->execute();
$rows = $stmt->fetch();

// get total no. of pages
$total_pages = ceil($rows[0]/$row_limit);



	if(isset($_SESSION['usuario'])){

		if($_SESSION['Tipo_usuario'] == "Admin"){
			




?>



<!DOCTYPE html>
<html>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../EMT.ico">

    <title>Panel EMT</title>

    <script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>


    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="js/jquery.bootpag.min.js" type="text/javascript"></script>



    <!-- Custom styles for this template -->

<style>
.main {
text-align:center;
}

</style>

</head>
<body>

    <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Electronic Music Travel</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="../index.php">Estadísticas</a></li>
       <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Pasajeros <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="../pasajeros/index.php">Últimos pasajeros</a></li>
            <li class="divider"></li>
            <?php 
                                      $info = new CRUD();
                                      $paquetes = $info->MenuPaquetes();


                                      if (count($paquetes) > 0) {
                                              foreach ($paquetes as $paquete) {
                                                    echo '<li><a href=pasajeros/' . strtolower($paquete['nombre']) . '.php>' . $paquete['descripcion'] . '</a></li>';
                                                                              }
                                                                }
                                      


                      ?>
          </ul>
        </li>
         <li><a href="../historico/index.php">Histórico</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li class="active"><a href="#"><span class="glyphicon glyphicon-user"></span><?php echo $_SESSION['Nombre']; echo '&nbsp'; echo $_SESSION['Apellido'];  ?></a></li>
        <li><a href="../../logout.php"><span class="glyphicon glyphicon-log-in"></span> Cerrar sesión</a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="container main">


<div class="panel panel-info">
<div class="panel-heading">Estadísticas</div>

<ul class="list-group">

  <li class="list-group-item list-group-item">
    <span class="badge"><?php echo $cant_emitidos ?></span>
    Paquetes vendidos y emitidos
  </li>

  <li class="list-group-item list-group-item">
    <span class="badge badge-danger">$<?php echo $rentabilidad ?></span>
    Ganancia total
  </li>
   <li class="list-group-item list-group-item">
    <span class="badge badge-danger">$<?php echo $rentabilidad_prom ?></span>
    Ganancia promedio por pasajero
  </li>
</ul>


</div>


</div>




    <div class="panel panel-default">
        <div class="panel-heading text-center"><h3>Histórico paquetes vendidos</h3></div>
				<div class="table-responsive">
				        <table charset="utf-8" class="table table-condensed">
				            <thead>
				                <tr>
				                    <th>Nombre</th>
				                    <th>Apellido</th>
				                    <th>Télefono</th>
				                    <th>Email</th>
				                    <th>Paquete</th>
				                </tr>
				            </thead>
				            <tbody id="pg-results">
				            </tbody>
				        </table>

				</div>
	</div>
        <div class="panel-footer text-center">
            <div class="pagination"></div>
        </div>
 </div>

    



<script type="text/javascript">
$(document).ready(function() {
    $("#pg-results").load("fetch_data.php");
    $(".pagination").bootpag({
        total: <?php echo $total_pages; ?>,
        page: 1,
        maxVisible: 5
    }).on("page", function(e, page_num){
        e.preventDefault();
        /*$("#results").prepend('<div class="loading-indication"><img src="ajax-loader.gif" /> Loading...</div>');*/
        $("#pg-results").load("fetch_data.php", {"page": page_num});
    });
});
</script>

</body>




  </body>
</html>

<?php

}else {
	
	header("Location: ../../");
}

} else {
	header("Location: ../../");
}


?>