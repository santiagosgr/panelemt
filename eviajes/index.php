<?php

  session_start();
  //require '../Admin/pasajeros/ajax/lib.php';

include 'db.php';
include 'lib.php';



  if(isset($_SESSION['usuario'])){

    if($_SESSION['Tipo_usuario'] == "eviajes"){
      

?>



<!DOCTYPE html>
<html>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../EMT.ico">

    <title>Panel EMT</title>

    <script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>


<script type="text/javascript" src="js/script.js"></script>



    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>



    

  <style>

html, body{ font-family: 'Helvetica', sans-serif; background-color:#fafafa; color: #7A7A7A; }
.main {font-size:15px; text-align:center;  }




</style>
<script>
$(document).ready(function () {
    // READ records on page load
    readRecords('SinEmitir'); // calling function  
    $('#show_cuotas').prop('readonly', true);
    $('#show_tarjeta').prop('disabled', true);
    $('#datepago').hide();
});

</script>

  </head>
  <body>




    <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Electronic Music Travel</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">General</a></li>
        <li><a href="emitidos.php">Emitidos</a></li>
        <li><a href="buscador.php">Buscador</a></li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Pasajeros <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="paquetes.php">Paquetes</a></li>
            <li class="divider"></li>
                        <?php 
                                      $info = new CRUD();
                                      $paquetes = $info->MenuPaquetes();


                                      if (count($paquetes) > 0) {
                                              foreach ($paquetes as $paquete) {
                                                    echo '<li><a href=paquetes/' . strtolower($paquete['nombre']) . '.php>' . $paquete['descripcion'] . '</a></li>';
                                                                              }
                                                                }
                                      


                      ?>
            <li class="divider"></li>
          </ul>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-user"></span><?php echo $_SESSION['Nombre']; echo '&nbsp'; echo $_SESSION['Apellido'];  ?></a></li>
        <li><a href="../logout.php"><span class="glyphicon glyphicon-log-in"></span> Cerrar sesión</a></li>
      </ul>
    </div>
  </div>
</nav>





<div class="container-fluid">


<div class="row">
<div class="col-sm-12 col-sm-offset- col-md-12-offset-2 main">




<div class="records_content"></div>



<!-- Modal - Update User details -->
<div class="modal fade" id="update_user_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Emitir paquete</h4>
            </div>
            <div class="modal-body">


     <!--
                <div class="col-lg-4 col-lg-offset-4 input-group has-warning">
                    
                    <div class="col-sm-9">
                    <input type="number" id="update_pcosto" placeholder="" class="form-control" required />
                     <span class="glyphicon glyphicon-warning-sign form-control-feedback"></span>
                     </div>
                </div>
                -->
                <h4>Precio de emisión</h4>
                <form class="form-inline">
                  <div class="form-group has-success">
                    <label class="sr-only" for="update_pcosto">Precio de emisión</label>
                    <div class="input-group">
                      <div class="input-group-addon">$</div>
                      <input type="text" class="form-control" id="update_pcosto">
                      </div>
                  </form>
                  <form>    
                    
                    <div class="input-group" id="nombrecuotas">

                      <div class="input-group-addon" >Cuotas</div>
                      <input type="number" class="form-control" id="show_cuotas" min="1" max="12" readonly>
                    </div>
                    <label class="sr-only" for="update_pcosto">Tarjeta</label>
                    <div class="input-group">



                      <!--
                      <div class="input-group-addon">Tarjeta</div>
                      <input type="text" class="form-control" id="show_tarjeta" readonly>
                    -->
                      <select id="show_tarjeta" class="selectpicker">
                      <?php 
                                      $info = new CRUD();
                                      $paquetes = $info->ConsultarTarjetas();


                                      if (count($paquetes) > 0) {
                                              foreach ($paquetes as $paquete) {
                                                                                      echo '<option>' . $paquete['nombre'] . '</option>';
                                                                              }
                                                                }
                                      


                      ?>
                    </optgroup>
                  </select>
                      
                    </div>
                  </div>
                <p class="" id="datepago"></span></p>
                </form>


            <h3>Datos personales</h3>
            <form class="form-inline">
              <div class="form-group">
                <label for="show_first_name">Nombre</label>
                <input type="text" class="form-control" id="show_first_name" readonly>
              </div>
              <div class="form-group">
                <label for="show_last_name">Apellido</label>
                <input type="text" class="form-control" id="show_last_name" readonly>
              </div>
              <div class="form-group">
                <label for="show_fnac">F.Nac.</label>
                <input type="date" class="form-control" id="show_fnac" readonly>
              </div>
              <div class="form-group">
                <label for="show_dni">DNI</label>
                <input type="number" class="form-control" id="show_dni" readonly>
              </div>
              
              <div class="form-group">
                <label for="show_email">Email</label>
                <input type="email" class="form-control" id="show_email" readonly>
              </div>
              <div class="form-group">
                <label for="show_tel">Tel</label>
                <input type="number" class="form-control" id="show_tel" readonly>
              </div>
               <div class="form-group">
                <label for="show_familiar">Familiar</label>
                <input type="text" class="form-control" id="show_familiar" readonly>
              </div>
              <div class="form-group">
                <label for="show_telseg">Tel Seg</label>
                <input type="number" class="form-control" id="show_telseg" readonly>
              </div>

             
            </form>
           
            <h3>Datos del viaje</h3>

            <form>
              <div class="form-group">
                <label for="show_paq">Paquete</label>
                <input type="text" class="form-control" id="show_paq" readonly>
              </div>
             </form>


             <form class="form-inline">


        <div class="form-group">
          <label class="sr-only" for="show_pcosto">VENTA</label>
          <div class="input-group">
            <div class="input-group-addon">$VENTA</div>
            <input type="number" class="form-control" id="show_pventa" readonly>
          </div>
        </div>


        <div class="form-group">
          <label class="sr-only" for="show_dif">Vendedor</label>
          <div class="input-group">
            <div class="input-group-addon">Vendedor</div>
            <input type="text" class="form-control" id="show_vendedor" readonly>
          </div>
        </div>

        <div class="form-group">
              <label class="sr-only" for="show_tipopago">Pago</label>
              <div class="input-group">
                <div class="input-group-addon">Pago</div>
                <input type="text" class="form-control" id="show_tipopago" readonly>
              </div>
            </div>

       



                



            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                <button type="button" id="emitir" class="btn btn-success" onclick="ActualizarDatos()" >Emitir</button>
                <input type="hidden" id="hidden_user_id2">
                <input type="hidden" id="paqID">
                <input type="hidden" id="pagoID">
                <input type="hidden" id="cuotas">
                <input type="hidden" id="tarjeta">
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
  

  $('#update_user_modal').on('hidden.bs.modal', function (e) {
  $("#update_pcosto").val('');
            $("#show_first_name").val('');
            $("#show_last_name").val('');
            $("#show_email").val('');
            $("#show_fnac").val('');
            $("#show_tel").val('');
            $("#show_telseg").val('');
            $("#show_paq").val('');
            $("#show_pventa").val('');
            //$("#show_pcosto").val(user.pcosto);
            $("#show_dni").val('');
            $("#show_vendedor").val('');
            $("#show_familiar").val('');
            $("#show_tipopago").val('');
            $('#datepago').hide(); 
            $('#show_cuotas').hide();
            $('#nombrecuotas').hide();  
            $('#show_tarjeta').hide();
            $('#show_cuotas').val(null);
            $('#show_tarjeta').val(null);
})
</script>






</div> <!-- col -->
</div> <!-- row -->
</div> <!-- container -->

  </body>
</html>

<?php

}else {
  
  header("Location: ../Admin/index.php");
}

} else {
  header("Location: ../../");
}


?>