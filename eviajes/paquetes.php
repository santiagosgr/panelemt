<?php
require '../Admin/pasajeros/ajax/lib.php';
	session_start();

	if(isset($_SESSION['usuario'])){

		if($_SESSION['Tipo_usuario'] == "eviajes"){
			

?>



<!DOCTYPE html>
<html>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../EMT.ico">

    <title>Panel EMT</title>

    <script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>


  <script type="text/javascript">



    
function AgregarPaquete() {

var nombre = $('#nombre').val();
var alias = $('#alias').val();

$.post("../pasajeros/agregarpaq.php", {


                        nombre: nombre,
                        alias: alias

                    }, function (data, status) {
                      location.reload();
                        
                       
                        

                      });

}

function ExportarPDF(parametro) {



window.open("exportar/pdf.php?parametro="+parametro, '_blank');

}




function ExportarExcel(parametro)
{

      window.open("../Admin/paquetes/export.php?parametro="+parametro, '_blank');

}



$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});





  </script>






    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


    

  <style>

html, body{ font-family: 'Helvetica', sans-serif; background-color:#fafafa; color: #7A7A7A; }
.main {font-size:15px; text-align:center;  }




</style>


  </head>
  <body>




    <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Electronic Music Travel</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="index.php">General</a></li>
        <li ><a href="emitidos.php">Emitidos</a></li>
        <li><a href="buscador.php">Buscador</a></li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Pasajeros <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li class="active"><a href="#">Paquetes</a></li>
            <li class="divider"></li>
                        <?php 
                                      $info = new CRUD();
                                      $paquetes = $info->MenuPaquetes();


                                      if (count($paquetes) > 0) {
                                              foreach ($paquetes as $paquete) {
                                                    echo '<li><a href=paquetes/' . strtolower($paquete['nombre']) . '.php>' . $paquete['descripcion'] . '</a></li>';
                                                                              }
                                                                }
                                      


                      ?>
            <li class="divider"></li>
          </ul>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-user"></span><?php echo $_SESSION['Nombre']; echo '&nbsp'; echo $_SESSION['Apellido'];  ?></a></li>
        <li><a href="../logout.php"><span class="glyphicon glyphicon-log-in"></span> Cerrar sesión</a></li>
      </ul>
    </div>
  </div>
</nav>




<table class="table table-bordered table-striped table-condensed" width="100%">
            <tr>
              <th>Nombre</th>
              <th>Exportar</th>
              <th>Link</th>
            </tr>

<?php

$object = new CRUD();

$paquetes = $object->ConsultaPaquetesDisponiblesTodos();

foreach ($paquetes as $paquete) {
  echo '<tr>';
  echo '<td>' . $paquete['descripcion'] . '</td>';
  echo '<td><a data-toggle="tooltip" title="Exportar a PDF" onclick="ExportarPDF(\'' . $paquete['nombre'] . '\')"><span class="glyphicon glyphicon-book" aria-hidden="true"></span></a><a data-toggle="tooltip" title="Exportar a Excel" onclick="ExportarExcel(\'' . $paquete['nombre'] . '\')"><span class="glyphicon glyphicon-export" aria-hidden="true"></span></a></td>';
  echo '<td><a href="paquetes/' . strtolower($paquete['nombre']) .'.php" class="btn btn-default">Ver pasajeros</a></td>';
  echo '</tr>';
  
}




?>
</table>








  </body>
</html>

<?php

}else {
	
	 header("Location: ../");
}

} else {
	header("Location: ../");
}


?>