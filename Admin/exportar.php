<?php
require 'pasajeros/ajax/lib.php';
	session_start();

	if(isset($_SESSION['usuario'])){

		if($_SESSION['Tipo_usuario'] == "Admin"){
			

?>



<!DOCTYPE html>
<html>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../img/EMT.ico">

    <title>Panel EMT</title>

    <script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
  <script
  src="js/main.js"></script>
  




    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <!-- Custom styles for this template -->
    <link href="../../css/dashboard.css" rel="stylesheet">
    

  


  </head>
  <body>


    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Electronic Music Travel</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
          	<li><a>Hola <?php echo $_SESSION['usuario'] ?></a></li>
            <li><a href="index.php">Dashboard</a></li>
            <li><a href="/PanelEMT/Main_app/Admin/pasajeros/index.php">Pasajeros</a></li>
            <li><a href="#">Exportar/Borrar</a></li>
            <li><a href="../logout.php">Cerrar sesión</a></li>
          </ul>
          
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li><a href="index.php">General <span class="pull-right glyphicon glyphicon-info-sign"></span></a></li>
            <li><a href="/PanelEMT/Main_app/Admin/pasajeros/index.php">Pasajeros <span class="pull-right hidden-xs showopacity glyphicon glyphicon-user badge"></a></li>
            <li class="active"><a href="#">Exportar/Borrar<span class="pull-right glyphicon glyphicon-export"></span></a></li>
          </ul>
          
        </div>
      </div>




<div class="container-fluid">
<div class="row">


  <div class="col-xs-10 col-sm-12 col-md-12 col-lg-10 center-block" style="text-align:center;">
    
  


<form>
  
 <select id="paquetes" class="selectpicker">
  <optgroup label="Seleccionar paquete a exportar o eliminar">
  <option>Hernan17</option>
  <option">Ultra17</option>
  <option>Miami18</option>
  <option>EuroTrip</option>
  </optgroup>
</select>

<script>
var a = $("#paquetes option:selected" ).text();

$('#paquetes').on('change', function() {
   a = $("#paquetes option:selected" ).text();
  
})
</script>
 <button onclick="Export(a)" id="enviar" class="btn btn-success">Exportar a Excel</button>
 <button onclick="Eliminar(a)" id="borrar" class="btn btn-danger">Borrar paquetes</button>
  
</form>



</div> <!-- col -->
</div> <!-- row -->
</div> <!-- container -->





  </body>
</html>

<?php

}else {
	
	header("Location: ../../");
}

} else {
	header("Location: ../../");
}


?>