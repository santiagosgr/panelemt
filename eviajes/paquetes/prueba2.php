<?php

require '../../Admin/pasajeros/ajax/lib.php';
    session_start();

    if(isset($_SESSION['usuario'])){

        if($_SESSION['Tipo_usuario'] == "eviajes"){


          $object = new CRUD();

          

?>



<!DOCTYPE html>
<html>
<!DOCTYPE html>
<html lang="en">
  <head>

  
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../EMT.ico">

    <title>Panel EMT</title>


 <!-- Jquery JS file -->
<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
 
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
 
<!-- Custom JS file -->
<script type="text/javascript" src="script.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.5.16/clipboard.min.js"></script>




<script>
$(document).ready(function () {
    // READ records on page load
    readRecordsPaquetes(location.href.match(/([^\/]*)\/*$/)[1].slice(0, -4)); // calling function



    
});


</script>

 <style>

html, body{ font-family: 'Helvetica', sans-serif; background-color:#fafafa; color: #7A7A7A; }
.main {font-size:15px; text-align:center;  }




</style>




    <!-- Custom styles for this template -->



  </head>

  <body>

     <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Electronic Music Travel</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="../index.php">General</a></li>
        <li><a href="../emitidos.php">Emitidos</a></li>
        <li><a href="../buscador.php">Buscador</a></li>
        <li class="dropdown active">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Pasajeros <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="../paquetes.php">Paquetes</a></li>
            <li class="divider"></li>
                        <?php 
                                      $info = new CRUD();
                                      $paquetes = $info->MenuPaquetes();


                                      if (count($paquetes) > 0) {
                                              foreach ($paquetes as $paquete) {
                                                    echo '<li><a href=' . strtolower($paquete['nombre']) . '.php>' . $paquete['descripcion'] . '</a></li>';
                                                                              }
                                                                }
                                      


                      ?>
            <li class="divider"></li>
          </ul>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-user"></span><?php echo $_SESSION['Nombre']; echo '&nbsp'; echo $_SESSION['Apellido'];  ?></a></li>
        <li><a href="../../logout.php"><span class="glyphicon glyphicon-log-in"></span> Cerrar sesión</a></li>
      </ul>
    </div>
  </div>
</nav>


<div class="container-fluid">
               

      <div class="row">
          <div class="col-sm-12 col-sm-offset- col-md-12-offset-2 main">


<div class="panel panel-warning">
  <!-- Default panel contents -->
  <div class="panel-heading">Vendedores</div>
  <!-- Table -->
  <table class="table table-condensed">

    <tbody>
      <tr class="active">
        <td>Vendedor</td>
        <td>Paq. vendidos</td>
        <td>Ganancia</td>

      </tr>
      
      <?php 
                                      $info = new CRUD();
                                      $vendedores = $info->VendedoresDeAgencia($_SESSION['Agencia']);
                                      $paquete = pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME);

                                      if (count($vendedores) > 0) {
                                              foreach ($vendedores as $vendedor) {
                                                    echo '<tr>';
                                                    echo '<td>' . $vendedor['Nombre'] . '</td>';
                                                    echo '<td>' . $info->EmitidosPorPaqueteVendedor($paquete,$vendedor['Nombre']) . '</td>';
                                                    echo '<td> $' . $info->RentabilidadPaqueteVendedor($paquete,$vendedor['Nombre']) . '</td>';
      
                                                    echo '</tr>';
                                                                              }
                                                                }
                                      


                      ?>
    </tbody>
  </table>
</div>


          
<div class="panel panel-default">



    
        <!--<h3>Últimos pasajeros cargados</h3>  -->
        <div class="table-responsive">
          <div class="records_content"></div> 
        </div>

</div>




      </div>
</div>



<!-- /Content Section -->



<!-- Modal - Update User details -->
<div class="modal fade" id="update_user_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Emitir paquete</h4>
            </div>
            <div class="modal-body">


     <!--
                <div class="col-lg-4 col-lg-offset-4 input-group has-warning">
                    
                    <div class="col-sm-9">
                    <input type="number" id="update_pcosto" placeholder="" class="form-control" required />
                     <span class="glyphicon glyphicon-warning-sign form-control-feedback"></span>
                     </div>
                </div>
                -->
                <h4>Precio de emisión</h4>
                <form class="form-inline">
                  <div class="form-group has-success">
                    <label class="sr-only" for="update_pcosto">Precio de emisión</label>
                    <div class="input-group">
                      <div class="input-group-addon">$</div>
                      <input type="text" class="form-control" id="update_pcosto">
                      </div>
                  </form>
                  <form>    
                    
                    <div class="input-group">

                      <div class="input-group-addon">Cuotas</div>
                      <input type="number" class="form-control" id="show_cuotas" min="1" max="12" readonly>
                    </div>
                    <label class="sr-only" for="update_pcosto">Tarjeta</label>
                    <div class="input-group">
                      <!--
                      <div class="input-group-addon">Tarjeta</div>
                      <input type="text" class="form-control" id="show_tarjeta" readonly>
                    -->
                      <select id="show_tarjeta" class="selectpicker">
                      <?php 
                                      $info = new CRUD();
                                      $paquetes = $info->ConsultarTarjetas();


                                      if (count($paquetes) > 0) {
                                              foreach ($paquetes as $paquete) {
                                                                                      echo '<option>' . $paquete['nombre'] . '</option>';
                                                                              }
                                                                }
                                      


                      ?>
                    </optgroup>
                  </select>
                      
                    </div>
                  </div>
                
                </form>


            <h3>Datos personales</h3>
            <form class="form-inline">
              <div class="form-group">
                <label for="show_first_name">Nombre</label>
                <input type="text" class="form-control" id="show_first_name" readonly>
              </div>
              <div class="form-group">
                <label for="show_last_name">Apellido</label>
                <input type="text" class="form-control" id="show_last_name" readonly>
              </div>
              <div class="form-group">
                <label for="show_fnac">F.Nac.</label>
                <input type="date" class="form-control" id="show_fnac" readonly>
              </div>
              <div class="form-group">
                <label for="show_dni">DNI</label>
                <input type="number" class="form-control" id="show_dni" readonly>
              </div>
              
              <div class="form-group">
                <label for="show_email">Email</label>
                <input type="email" class="form-control" id="show_email" readonly>
              </div>
              <div class="form-group">
                <label for="show_tel">Tel</label>
                <input type="number" class="form-control" id="show_tel" readonly>
              </div>
               <div class="form-group">
                <label for="show_familiar">Familiar</label>
                <input type="text" class="form-control" id="show_familiar" readonly>
              </div>
              <div class="form-group">
                <label for="show_telseg">Tel Seg</label>
                <input type="number" class="form-control" id="show_telseg" readonly>
              </div>

             
            </form>
           
            <h3>Datos del viaje</h3>

            <form>
              <div class="form-group">
                <label for="show_paq">Paquete</label>
                <input type="text" class="form-control" id="show_paq" readonly>
              </div>
             </form>


             <form class="form-inline">


        <div class="form-group">
          <label class="sr-only" for="show_pcosto">VENTA</label>
          <div class="input-group">
            <div class="input-group-addon">$VENTA</div>
            <input type="number" class="form-control" id="show_pventa" readonly>
          </div>
        </div>


        <div class="form-group">
          <label class="sr-only" for="show_dif">Vendedor</label>
          <div class="input-group">
            <div class="input-group-addon">Vendedor</div>
            <input type="text" class="form-control" id="show_vendedor" readonly>
          </div>
        </div>

        <div class="form-group">
              <label class="sr-only" for="show_tipopago">Pago</label>
              <div class="input-group">
                <div class="input-group-addon">Pago</div>
                <input type="text" class="form-control" id="show_tipopago" readonly>
              </div>
            </div>

       



                



            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" id="emitir" class="btn btn-primary" onclick="ActualizarDatos()" >Emitir</button>
                <input type="hidden" id="hidden_user_id2">
                <input type="hidden" id="paqID">
                <input type="hidden" id="pagoID">
                <input type="hidden" id="cuotas">
                <input type="hidden" id="tarjeta">
            </div>
        </div>
    </div>
</div>


<!-- Modal - Update User details -->
<div class="modal fade" id="show_user_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Datos Completos</h4>
            </div>
            <div class="modal-body">




            <!--
        $("#update_first_name").val(user.first_name);
            $("#update_last_name").val(user.last_name);
            $("#update_email").val(user.email);
            $("#update_fnac").val(user.fnac);
            $("#update_tel").val(user.tel);
            $("#update_telseg").val(user.telseg);
            $("#update_paq").val(user.paq);
            $("#update_pventa").val(user.pventa);
            $("#update_pcosto").val(user.pcosto);
            $("#update_dni").val(user.dni);
            $("#update_vendedor").val(user.vendedor);

            -->

            <h4>Datos personales</h4>
            <form class="form-inline">
              <div class="form-group">
                <label for="show_first_name">Nombre</label>
                <input type="text" class="form-control" id="show_first_name" readonly>
              </div>
              <div class="form-group">
                <label for="show_last_name">Apellido</label>
                <input type="text" class="form-control" id="show_last_name" readonly>
              </div>
              <div class="form-group">
                <label for="show_fnac">F.Nac.</label>
                <input type="date" class="form-control" id="show_fnac" readonly>
              </div>
              <div class="form-group">
                <label for="show_dni">DNI</label>
                <input type="number" class="form-control" id="show_dni" readonly>
              </div>
              
              <div class="form-group">
                <label for="show_email">Email</label>
                <input type="email" class="form-control" id="show_email" readonly>
              </div>
              <div class="form-group">
                <label for="show_tel">Tel</label>
                <input type="number" class="form-control" id="show_tel" readonly>
              </div>
               <div class="form-group">
                <label for="show_familiar">Familiar</label>
                <input type="text" class="form-control" id="show_familiar" readonly>
              </div>
              <div class="form-group">
                <label for="show_telseg">Tel Seg</label>
                <input type="number" class="form-control" id="show_telseg" readonly>
              </div>

             
            </form>
           
            <h4>Datos del viaje</h4>

    <form class="form-inline">
                 <div class="form-group">
              <label class="sr-only" for="show_paq">Paquete</label>
              <div class="input-group">
                <div class="input-group-addon">Paquete</div>
                <input type="text" class="form-control" id="show_paq" readonly>
              </div>
            </div>


              <div class="form-group">
          <label class="sr-only" for="show_pcosto">COSTO</label>
          <div class="input-group">
            <div class="input-group-addon">$COSTO</div>
            <input type="number" class="form-control" id="show_pcosto" readonly>
          </div>
        </div>

        <div class="form-group">
          <label class="sr-only" for="show_pcosto">VENTA</label>
          <div class="input-group">
            <div class="input-group-addon">$VENTA</div>
            <input type="number" class="form-control" id="show_pventa" readonly>
          </div>
        </div>

        <div class="form-group">
          <label class="sr-only" for="show_dif">Ganancia</label>
          <div class="input-group">
            <div class="input-group-addon">$ NETA</div>
            <input type="number" class="form-control" id="show_dif" readonly>
          </div>
        </div>

        <div class="form-group">
          <label class="sr-only" for="show_dif">Vendedor</label>
          <div class="input-group">
            <div class="input-group-addon">Vendedor</div>
            <input type="text" class="form-control" id="show_vendedor" readonly>
          </div>
        </div>

          <div class="form-group">
            
              <label class="sr-only" for="show_tipopago">Pago</label>
              <div class="input-group">
                <div class="input-group-addon">Pago</div>
                <input type="text" class="form-control" id="show_tipopago" readonly>
              </div>
            </div>
              
             
            </form>


            <script>
              var clipboard = new Clipboard('#copiar');

                clipboard.on('success', function(e) {
                    e.clearSelection();
                    //alert('Copy to Clipboard Successfully');
                });

                clipboard.on('error', function(e) {
                    //alert('Something is wrong!');
                });

            </script>
            <div id="botoneslinks">
              <h4>Generar links de pago</h4>
              <span id="user_id" style="display: none"></span>
              <button type="button" class="btn btn-info btn-xs" disabled="disabled" onclick="LinkMP(id)">MercadoPago</button>
              <button type="button" class="btn btn-info btn-xs" onclick="LinkTP(id)">TodoPago</button>
                <div class="input-group input-group-sm" id="prueba2">
                  <span class="input-group-addon" id="basic-addon1"><button type="button" data-clipboard-action="copy" class="btn btn-info btn-xs glyphicon glyphicon-file" id="copiar" data-clipboard-target="#linkmp" style="display: none"></button></span>
                    <input type="text" class="form-control" id="linkmp" style="display: none" readonly>
                </div>

            </div>
              

                

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="cerrarver" data-dismiss="modal">Cerrar</button>
                <input type="hidden" id="hidden2_user_id">
            </div>
        </div>
    </div>
</div>
<!-- // Modal -->



























        </div>
      </div>
    </div>




  </body>
</html>

<?php

}else {
    
    header("Location: ../../");
}

} else {
    header("Location: ../../");
}
?>