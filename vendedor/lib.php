<?php

require '../Admin/pasajeros/ajax/db_connection.php';

class CRUD
{

    public $db;

    function __construct()
    {
        $this->db = DB();
    }

    function __destruct()
    {
        $this->db = null;
    }

    /*
     * Add new Record
     *
     * @param $first_name
     * @param $last_name
     * @param $email
     * @return $mixed
     * */

   



    public function Read()
    {
        $query = $this->db->prepare("SELECT * FROM users order by id desc LIMIT 10");
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    public function PasajerosPaquete($paq)
    {
        $query = $this->db->prepare("SELECT * FROM users WHERE paq = :paq");
        $query->bindParam("paq", $paq, PDO::PARAM_STR);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }
    public function PasajerosPaqueteAgencia($paq,$eviajes)
    {
        $query = $this->db->prepare("SELECT * FROM users WHERE paq = :paq AND eviajes = :eviajes");
        $query->bindParam("paq", $paq, PDO::PARAM_STR);
        $query->bindParam("eviajes", $eviajes, PDO::PARAM_STR);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }
     public function PasajerosPorVendedor($vendedor)
    {
        $query = $this->db->prepare("SELECT * FROM users WHERE vendedor = :vendedor order by id DESC");
        $query->bindParam("vendedor", $vendedor, PDO::PARAM_STR);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }
    



public function Rentabilidad()
    {
        $query = $this->db->prepare("SELECT SUM(dif) AS sum_dif FROM users WHERE pcosto != 0");
        $query->execute();
        $resultado = $query -> fetch(PDO::FETCH_ASSOC);
        $total = $resultado['sum_dif'];
        return $total;
    }

    public function RentabilidadPaquete($paq)
    {
        $query = $this->db->prepare("SELECT SUM(dif) AS sum_dif FROM users WHERE paq = :paq AND pcosto != 0");
        $query->bindParam("paq", $paq, PDO::PARAM_STR);
        $query->execute();
        $resultado = $query -> fetch(PDO::FETCH_ASSOC);
        $total = $resultado['sum_dif'];
        return $total;
    }

    public function SinEmitir()
    {
        $query = $this->db->prepare("SELECT * FROM users WHERE pcosto = 0");
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }
    public function PasajerosEmitidosPorAgencia($eviajes)
    {
        $query = $this->db->prepare("SELECT * FROM users WHERE eviajes = :eviajes AND pcosto != 0");
        $query->bindParam("eviajes", $eviajes, PDO::PARAM_STR);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

     public function CantidadSinEmitir()
    {
        $query = $this->db->prepare("SELECT * FROM users WHERE pcosto = 0");
        $query->execute();
        $cantidad = $query->rowCount();
        return $cantidad;
    }

    public function EmitidosPorPaquete($paq)
    {
        $query = $this->db->prepare("SELECT * FROM users WHERE paq = :paq AND pcosto != 0");
        $query->bindParam("paq", $paq, PDO::PARAM_STR);
        $query->execute();
        $cantidad = $query->rowCount();
        return $cantidad;
    }

    public function Emitidos()
    {
        $query = $this->db->prepare("SELECT * FROM users WHERE pcosto != 0");
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    /*
     * Delete Record
     *
     * @param $user_id
     * */
    public function Delete($user_id)
    {
        $query = $this->db->prepare("DELETE FROM users WHERE id = :id");
        $query->bindParam("id", $user_id, PDO::PARAM_STR);
        $query->execute();
    }

    public function EliminarPaquetes($paq)
    {
        $query = $this->db->prepare("DELETE FROM users WHERE paq = :paq");
        $query->bindParam("paq", $paq, PDO::PARAM_STR);
        $query->execute();
    }


      public function TipoPago($user_id)
    {
        $query = $this->db->prepare("SELECT tipopago FROM users WHERE id = :id");
        $query->bindParam("id", $user_id, PDO::PARAM_STR);
        $query->execute();
        $row = $query->fetch(PDO::FETCH_ASSOC);
        return $row['tipopago'];
    }


    public function Update($pcosto, $user_id, $tipopago)
    {
    $query = $this->db->prepare("UPDATE users SET pcosto = :pcosto WHERE id = :id");
    $query->bindParam("pcosto", $pcosto, PDO::PARAM_INT);
    $query->bindParam("id", $user_id, PDO::PARAM_STR);
    $query->execute();

    //Actualizo el costo
        if ($tipopago === 'Tarjeta') {
            $query = $this->db->prepare("UPDATE users SET dif = (pventa-pcosto) - (pventa * 0.13) WHERE id = :id"); 
            $query->bindParam("id", $user_id, PDO::PARAM_STR);   
            $query->execute();
        } else {
            $query = $this->db->prepare("UPDATE users SET dif = (pventa-pcosto) WHERE id = :id"); 
            $query->bindParam("id", $user_id, PDO::PARAM_STR);   
            $query->execute();
        }

 
    }

    /*
     * Get Details
     *
     * @param $user_id
     * */
    public function Details($user_id)
    {
        $query = $this->db->prepare("SELECT * FROM users WHERE id = :id");
        $query->bindParam("id", $user_id, PDO::PARAM_STR);
        $query->execute();
        return json_encode($query->fetch(PDO::FETCH_ASSOC));
    }



  



}

?>