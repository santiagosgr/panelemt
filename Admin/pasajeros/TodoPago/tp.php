<?php
//importo archivo con SDK
//include_once dirname(__FILE__)."/TodoPago/autoload.php";
//use TodoPago\Sdk;
include 'lib/Sdk.php';
include '../ajax/lib.php';


	$idpaq = $_POST['idpaq'];
	$nom = $_POST['nombre'];
   $pventa = $_POST['pventa'];
   $dni = $_POST['dni'];
   $tel = $_POST['tel'];
   $correo = $_POST['email'];
   $ap = $_POST['ap'];
   $paq = $_POST['paq'];
   $coma = number_format($pventa, 2, '.', '');
   $opid = rand(1, 2147483647);

   $object = new CRUD();
   $object->OperationID($opid,$idpaq);


 if(isset($_POST['cuotas'])){

 	$cuotas = $_POST['cuotas'];

 	} else {
 	$cuotas = $object->ConsultarCuotasPaquete($paq);
 	}

   



$mode = "prod";//identificador de entorno obligatorio, la otra opción es "prod"
$merch_trip = 466729;
$merch_test_personal = 33892;
$http_header = array('Authorization'=>'TODOPAGO CC21EF42B33B4707DFC9204FB14D8002');// PROD TRIP
//$http_header = array('Authorization'=>'TODOPAGO 13df0053e536466484bb36f4ea2a8bd9');// TEST PERSONAL

$connector = new TodoPago\Sdk($http_header, $mode);

$optionsSAR_comercio = array (
	'Security'=> 'CC21EF42B33B4707DFC9204FB14D8002',
	'EncodingMethod'=>'XML',
	'Merchant'=>466729,
	'URL_OK'=>'https://electronicmusictravel.com/pagos/ok.php?mail=' .$correo. '&nom=' .$nom,

	'URL_ERROR'=>'https://electronicmusictravel.com/pagos/error.html'
);

$optionsSAR_operacion = array (
	'MERCHANT'=>466729, //dato fijo (número identificador del comercio)
	'TIMEOUT'=> 21600000,
	'MININSTALLMENTS'=>1,
	'MAXINSTALLMENTS'=>$cuotas,
	'OPERATIONID'=>$opid, //número único que identifica la operación, generado por el comercio.
	'CURRENCYCODE'=> 32, //por el momento es el único tipo de moneda aceptada
	'AMOUNT'=>floatval($pventa),
	'EMAILCLIENTE'=>$correo,
	'CSBTCITY'=>'Ciudad Autonoma de Buenos Aires', //Ciudad de facturación, REQUERIDO.
	'CSBTCOUNTRY'=>'AR', //País de facturación. REQUERIDO. Código ISO.
	'CSBTCUSTOMERID'=>'453458', //Identificador del usuario al que se le emite la factura. REQUERIDO. No puede contener un correo electrónico.
	'CSBTIPADDRESS'=>$_SERVER['REMOTE_ADDR'], //IP de la PC del comprador. REQUERIDO.
	'CSBTEMAIL'=>$correo,
	'CSSTEMAIL'=>$correo, //Mail del usuario al que se le emite la factura. REQUERIDO.
	'CSBTFIRSTNAME'=>$nom,//Nombre del usuario al que se le emite la factura. REQUERIDO.
	'CSBTLASTNAME'=>$ap, //Apellido del usuario al que se le emite la factura. REQUERIDO.
	'CSBTPHONENUMBER'=>'54' .$tel , //Teléfono del usuario al que se le emite la factura. No utilizar guiones, puntos o espacios. Incluir código de país. REQUERIDO.
	'CSBTPOSTALCODE'=>'C1006ACL', //Código Postal de la dirección de facturación. REQUERIDO.
	'CSBTSTATE'=>'C', //Provincia de la dirección de facturación. REQUERIDO. Ver tabla anexa de provincias.
	'CSBTSTREET1'=>'Maipu 812', //Domicilio de facturación (calle y nro). REQUERIDO.
	'CSBTSTREET2'=>'Piso 7 Oficna G', //Complemento del domicilio. (piso, departamento). OPCIONAL.
	'CSPTCURRENCY'=>'ARS', //Moneda. REQUERIDO.
	'CSPTGRANDTOTALAMOUNT'=>'125.38', //Con decimales opcional usando el punto como separador de decimales. No se permiten comas, ni como separador de miles ni como separador de decimales. REQUERIDO. (Ejemplos:$125,38-> 125.38 $12-> 12 o 12.00)
	'CSMDD7'=>'', // Fecha registro comprador(num Dias). OPCIONAL.
	'CSMDD8'=>'Y', //Usuario Guest? (Y/N). En caso de ser Y, el campo CSMDD9 no deberá enviarse. OPCIONAL.
	'CSMDD9'=>'', //Customer password Hash: criptograma asociado al password del comprador final. OPCIONAL.
	'CSMDD10'=>'', //Histórica de compras del comprador (Num transacciones). OPCIONAL.
	'CSMDD11'=>'', //Customer Cell Phone. OPCIONAL.
	'CSSTCITY'=>'Buenos Aires', //Ciudad de envío de la orden. REQUERIDO.
	'CSSTCOUNTRY'=>'AR', //País de envío de la orden. REQUERIDO.
	'CSSTFIRSTNAME'=>$nom, //Nombre del destinatario. REQUERIDO.
	'CSSTLASTNAME'=>$ap, //Apellido del destinatario. REQUERIDO.
	'CSSTPHONENUMBER'=>'54' .$tel , //Número de teléfono del destinatario. REQUERIDO.
	'CSSTPOSTALCODE'=>'1414', //Código postal del domicilio de envío. REQUERIDO.
	'CSSTSTATE'=>'D', //Provincia de envío. REQUERIDO. Son de 1 caracter
	'CSSTSTREET1'=>'Mauipu 812', //Domicilio de envío. REQUERIDO.
	'CSMDD12'=>'',//Shipping DeadLine (Num Dias). NO REQUERIDO.
	'CSMDD13'=>'',//Método de Despacho. NO REQUERIDO.
	'CSMDD14'=>'',//Customer requires Tax Bill ? (Y/N). NO REQUERIDO.
	'CSMDD15'=>'',//Customer Loyality Number. NO REQUERIDO.
	'CSMDD16'=>'',//Promotional / Coupon Code. NO REQUERIDO.
	//Retail: datos a enviar por cada producto, los valores deben estar separados con #:
	'CSITPRODUCTCODE'=>'electronic_good', //Código de producto. REQUERIDO. Valores posibles(adult_content;coupon;default;electronic_good;electronic_software;gift_certificate;handling_only;service;shipping_and_handling;shipping_only;subscription)
	'CSITPRODUCTDESCRIPTION'=>'ELECTONIC MUSIC TRAVEL PAQUETE', //Descripción del producto. REQUERIDO.
	'CSITPRODUCTNAME'=>$paq, //Nombre del producto. REQUERIDO.
	'CSITPRODUCTSKU'=>'LEVJNSL36GN', //Código identificador del producto. REQUERIDO.
	'CSITTOTALAMOUNT'=>$coma, //CSITTOTALAMOUNT=CSITUNITPRICE*CSITQUANTITY "999999[.CC]" Con decimales opcional usando el punto como separador de decimales. No se permiten comas, ni como separador de miles ni como separador de decimales. REQUERIDO.
	'CSITQUANTITY'=>'1', //Cantidad del producto. REQUERIDO.
	'CSITUNITPRICE'=>$coma, //Formato Idem CSITTOTALAMOUNT. REQUERIDO.
	);


$values = $connector->sendAuthorizeRequest($optionsSAR_comercio, $optionsSAR_operacion);

function AcortarLink($linkTP) {
 	$url=rawurlencode($linkTP);
    $usuario = "00725a391c7b7bc83914b55b68277a6783e6f402";
    $apikey = "6a3ced2a77956cb9cd87ee68134f7414f9cc733f";
    $tempo = "https://api-ssl.bitly.com/v3/shorten?access_token=6a3ced2a77956cb9cd87ee68134f7414f9cc733f&longUrl=".$url."&format=txt";
    return file_get_contents($tempo);
}


if($values['StatusCode'] != -1) {
	var_dump($values);
} else {
	//setcookie('RequestKey',$values["RequestKey"],  time() + (86400 * 30), "/");
	$data = $values["URL_Request"];	
	$urlacortada = AcortarLink($data);
	echo $urlacortada;
	//echo $data;
}