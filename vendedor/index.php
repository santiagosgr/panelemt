<?php

require '../Admin/pasajeros/ajax/lib.php';
    session_start();

    if(isset($_SESSION['usuario'])){

        if($_SESSION['Tipo_usuario'] == "vendedor"){
            

?>



<!DOCTYPE html>
<html>
<!DOCTYPE html>
<html lang="en">
  <head>

  
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../EMT.ico">

    <title>Panel EMT</title>


 <!-- Jquery JS file -->
<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
 
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


 
<!-- Custom JS file -->
<script type="text/javascript" src="js/script.js"></script>
<!-- <script type="text/javascript" src="../js/main.js"></script> -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.5.16/clipboard.min.js"></script>






    <!-- Custom styles for this template -->


<style>
     #buscar {
        border: 0;
        background: none;
        /** belows styles are working good */
        padding: 2px 5px;
        margin-top: 2px;
        position: relative;
        left: -28px;
        /* IE7-8 doesn't have border-radius, so don't indent the padding */
        margin-bottom: 0;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        color:#D9230F;
    }
</style>


  </head>



  <body>
  <script>
$(document).ready(function () {
    // READ records on page load
    readRecords('Vendedor'); // calling function  
    $("#errorval").hide();
    $("#paxnotfound").hide();


});

$("#dnibuscar").keyup(function(event) {
    if (event.keyCode === 13) {
        $("#btnbuscar").click();
        alert('Enter');
    }
});




</script>



   <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Electronic Music Travel</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">General</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-user"></span><?php echo $_SESSION['Nombre']; echo '&nbsp'; echo $_SESSION['Apellido'];echo ' : ';echo $_SESSION['Agencia'];  ?></a></li>
        <li><a href="../logout.php"><span class="glyphicon glyphicon-log-in"></span> Cerrar sesión</a></li>
      </ul>
    </div>
  </div>
</nav>








<div class="container-fluid">

<!--
            <div class="pull-left">
                <button class="btn btn-success" data-toggle="modal" data-target="#add_new_record_modal">Añadir pasajero</button>
            </div>
            -->


               

    <div class="row">
        <div class="col-sm-12 col-sm-offset- col-md-12-offset-2 main">
        



<div class="panel panel-default">
        <div class="panel-heading text-center"><h3>Últimos pasajeros</h3><br>
        <button type="button" class="btn btn-info" data-toggle="modal" href="#add_new_record_modal">Agregar</button>


        



        </div>    
        <!--<h3>Últimos pasajeros cargados</h3>  -->
        <div class="table-responsive">
          <div class="records_content"></div> 
        </div>

</div>

</div>



</div>
</div>






<!-- Modal -->
<div class="modal fade" id="exportar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Exportar pasajeros</h4>
      </div>
      <div class="modal-body">
        
            <form>  
             <select id="paquetes" class="selectpicker">
              <optgroup label="Seleccionar paquete a exportar">
<?php 
                $info = new CRUD();
                $paquetes = $info->ConsultaPaquetesDisponibles();


                if (count($paquetes) > 0) {
                        foreach ($paquetes as $paquete) {
                                                                echo '<option>' . $paquete['nombre'] . '</option>';
                                                        }
                                          }
                


?>
              </optgroup>
            </select>

            <script>
            var a = $("#paquetes option:selected" ).text();

            $('#paquetes').on('change', function() {
               a = $("#paquetes option:selected" ).text();
              
            })

            </script>



      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button onclick="" id="enviar" class="btn btn-success">Exportar a Excel</button>

      </div>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="eliminar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Eliminar pasajeros</h4>
      </div>
      <div class="modal-body">
        
            <form>
  
             <select id="paquetes2" class="selectpicker">
              <optgroup label="Seleccionar paquete a eliminar">
<?php 
                $info = new CRUD();
                $paquetes = $info->ConsultaPaquetesDisponibles();


                if (count($paquetes) > 0) {
                        foreach ($paquetes as $paquete) {
                                                                echo '<option>' . $paquete['nombre'] . '</option>';
                                                        }
                                          }
                


?>
              </optgroup>
            </select>

            <script>
            var a = $("#paquetes2 option:selected" ).text();

            $('#paquetes2').on('change', function() {
               a = $("#paquetes2 option:selected" ).text();
              
            })

            </script>



      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button onclick="Eliminar(a)" id="enviar" class="btn btn-success">Eliminar</button>

      </div>
    </div>
  </div>
</div>



<!-- /Content Section -->

<!-- Bootstrap Modals -->
<!-- Modal - Add New Record/User -->
<div class="modal fade" id="add_new_record_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" id="addpasajeros">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Añadir nuevo pasajero</h4>
            </div>
            <div class="modal-body">



               <form class="form-inline">

                  <div class="form-group has-success">
                    <div class="input-group">
                      <div class="input-group-addon">Buscar</div>
                      <input type="text" class="search-query form-control" placeholder="Busqueda por DNI o apellido..." id="dnibuscar">
                      
                      <span class="input-group-btn">
                                    <button class="btn btn-success" type="button" onclick="Buscar()" id="btnbuscar">
                                        <span class=" glyphicon glyphicon-search"></span>
                                    </button>
                                </span>
                      
                    </div>
                  </div>
                  <p class="text-danger" id="paxnotfound"><b>Error!</b>Usuario no encontrado</p>

                  <script type="text/javascript">

                    $('#dnibuscar').keypress(function (e) {
             var key = e.which;
             if(key == 13)  // the enter key code
              {
                $('#btnbuscar').click();
                return false;  
              }
            });
                  </script>


                

            <form id="agregarpasajero">
                <div class="form-group required">
                    <label for="first_name">Nombre</label>
                    <input type="text" id="first_name" placeholder="" class="form-control" required />
                </div>

                <div class="form-group required">
                    <label for="last_name">Apellido</label>
                    <input type="text" id="last_name" placeholder="" class="form-control" required="" />
                </div>

                <div class="form-group required">
                    <label for="dni">DNI</label>
                    <input type="number" id="dni" placeholder="" class="form-control" required />
                </div>

                <div class="form-group required">
                    <label for="email">Email</label>
                    <input type="email" id="email" placeholder="" class="form-control" required />
                </div>


                <div class="form-group required">
                    <label for="fnac">Fecha de nacimiento</label>
                    <input type="date" id="fnac" placeholder="Email" class="form-control" required />
                </div>

                <div class="form-group required">
                    <label for="tel">Telefono</label>
                    <input type="number" id="tel" placeholder="" class="form-control" required />
                </div>


                <hr>
                <h4 class="text-center">Seguro médico</h4>
                <center>
                <div class="form-check form-check-inline">
                <label class="form-check-label text-center">
                  <input class="form-check-input" type="checkbox" id="sinsm" value="sinsm"> Sin seguro
                </label>
                </div>
                </center>


                <script type="text/javascript">
                  
                  $('#sinsm').change(function(){
                  if($(this).prop("checked")) {
                    $("#familiar").val('SIN SEGURO');
                    $('#familiar').prop('readonly', true);
                    $("#telseg").val(0);
                    $('#telseg').prop('readonly', true);
                  } else {
                    $("#familiar").val('');
                    $('#familiar').prop('readonly', false);
                    $("#telseg").val('');
                    $('#telseg').prop('readonly', false);
                    
                  }
                });


                </script>


                <div class="form-group required">
                    <label for="familiar">Familiar</label>
                    <input type="text" id="familiar" placeholder="" class="form-control" required />
                </div>

                <div class="form-group required">
                    <label for="telseg">Telefono seguro</label>
                    <input type="number" id="telseg" placeholder="" class="form-control" required />
                </div>
                
                 <hr>
                <h3 class="text-center">Datos del viaje</h3>
                <!--
                <div class="form-group">
                    <label for="paq">Paquete</label>
                    <input type="text" id="paq" placeholder="" class="form-control"/>
                </div>
                -->
                <div class="form-group required">
                <label for="paq">Paquete</label>
                <select id="paq" class="selectpicker" required>
                  <optgroup label="Seleccionar paquete">


<?php 
                $info = new CRUD();
                $paquetes = $info->ConsultaPaquetesDisponibles();


                if (count($paquetes) > 0) {
                        foreach ($paquetes as $paquete) {
                                                                echo '<option value='. $paquete['nombre'] . '>' . $paquete['descripcion'] . '</option>';
                                                        }
                                          }



?>


                  </optgroup>
                    </select>
                </div>


                <div class="form-group required">
                    <label for="pventa">$ Venta</label>
                    <input type="number" id="pventa" min="1" class="form-control" required />
                </div>

                <div class="form-group">
                <label for="tipopago">Medio de pago</label>
                        <select id="tipopago" class="selectpicker" onchange="">
                          <optgroup label="Seleccionar medio de pago">
                          <?php 
                $info = new CRUD();
                $paquetes = $info->ConsultaTipoPagosDisponibles();


                if (count($paquetes) > 0) {
                        foreach ($paquetes as $paquete) {
                                                                echo '<option>' . $paquete['nombre'] . '</option>';
                                                        }
                                          }



?>
                          </optgroup>
                        </select>
                </div>
                
                <!--
                <div class="form-group">
                    <label for="pcosto">$ Costo</label>
                    <input type="number" id="pcosto" placeholder="" class="form-control"/>
                </div>
                -->
                <div class="form-group required">
                <label for="eviajes">Agencia</label>
                        <select id="eviajes" class="selectpicker" required>
                          <optgroup label="Seleccionar agencia de viajes">
                          <option><? echo $_SESSION['Agencia']; ?></option>
                          </optgroup>
                        </select>
                </div>
                
                

                
                <!--
                <div class="form-group">
                    <label for="vendedor">Vendedor</label>
                    <input type="text" id="vendedor" placeholder="" class="form-control"/>
                </div>
                -->
                <div class="form-group required">
                <label for="vendedor">Vendedor</label>
                        <select id="vendedor" class="selectpicker" required>
                          <optgroup label="Seleccionar vendedor">
                          <?php 
                $data = new CRUD();
                $vendedores = $data->ConsultarVendedores();


                if (count($vendedores) > 0) {
                        foreach ($vendedores as $vendedor) {
                                                                echo '<option>' . $vendedor['Nombre'] . '</option>';
                                                        }
                                          }



                    ?>
                          </optgroup>
                        </select>
                </div>
                <!--
                <div class="form-group">
                    <label for="vendedor">Comprobante</label>
                    <input type="file" id="file" placeholder="" class="form-control"/><br>
                    <p class="bg-info">Subir con el siguiente formato: ApellidoNombrePasajero_Paquete</p>
                </div>
                -->
                
                <div class="alert alert-danger" id="errorval" role="alert"><b>ERROR:</b>Verificar campos en rojo.</div>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="cancelar">Cancelar</button>
               <button type="button" class="btn btn-primary" onclick="Verificar()" id="agregar">Añadir</button> 
               <!-- <button type="button" class="btn btn-primary" onclick="addRecord()" id="agregar">Añadir</button> -->

                
            </div>
            </form>
        </div>
    </div>
</div>
<!-- // Modal -->

<script type="text/javascript">
  

  $('#add_new_record_modal').on('hidden.bs.modal', function (e) {
        $('#sinsm').attr('disabled',false);
        $('#sinsm').prop('checked', false);
        $('#telseg').prop('readonly', true);
        $('#first_name').prop('readonly', false);
        $('#first_name').val('');
        $('#last_name').prop('readonly', false);
        $('#last_name').val('');
        $('#email').prop('readonly', false);
        $('#email').val('');
        $('#fnac').prop('readonly', false);
        $('#fnac').val('');
        $('#tel').prop('readonly', false);
        $('#tel').val('');
        $('#telseg').prop('readonly', false);
        $('#telseg').val('');
        $('#dni').prop('readonly', false);
        $('#dni').val('');
        $('#familiar').prop('readonly', false);
        $('#familiar').val('');
        $('#dnibuscar').val('');

});



</script>


<!-- Modal - Update User details -->
<div class="modal fade" id="update_user_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Actualizar datos</h4>
            </div>
            <div class="modal-body">
<form id="agregardatos">

                <div class="form-group">
                    <label for="update_first_name">Nombre</label>
                    <input type="text" id="update_first_name" placeholder="" class="form-control"/>
                </div>

                <div class="form-group">
                    <label for="update_last_name">Apellido</label>
                    <input type="text" id="update_last_name" placeholder="" class="form-control"/>
                </div>

                <div class="form-group">
                    <label for="update_dni">DNI</label>
                    <input type="number" id="update_dni" placeholder="" class="form-control"/>
                </div>

                <div class="form-group">
                    <label for="update_email">Email</label>
                    <input type="email" id="update_email" placeholder="" class="form-control"/>
                </div>


                <div class="form-group">
                    <label for="update_fnac">Fecha de nacimiento</label>
                    <input type="date" id="update_fnac"  class="form-control"/>
                </div>

                <div class="form-group">
                    <label for="update_tel">Telefono</label>
                    <input type="number" id="update_tel" placeholder="" class="form-control"/>
                </div>

                <div class="form-group">
                    <label for="familiar">Familiar</label>
                    <input type="text" id="update_familiar" placeholder="" class="form-control"/>
                </div>

                <div class="form-group">
                    <label for="update_telseg">Telefono seguro</label>
                    <input type="text" id="update_telseg" placeholder="" class="form-control"/>
                </div>
                <!--
                <div class="form-group">
                    <label for="update_paq">Paquete</label>
                    <input type="text" id="update_paq" placeholder="" class="form-control"/>
                </div>
                -->
                <div class="form-group">
                <label for="update_paq">Paquete</label>
                        <select id="update_paq" class="selectpicker" onchange="">
                          <optgroup label="Seleccionar paquete">
<?php 
                $info = new CRUD();
                $paquetes = $info->ConsultaPaquetesDisponibles();


                if (count($paquetes) > 0) {
                        foreach ($paquetes as $paquete) {
                                                                echo '<option>' . $paquete['nombre'] . '</option>';
                                                        }
                                          }
                


?>
                        </select>
                </div>

                <div class="form-group">
                    <label for="update_pventa">$Venta</label>
                    <input type="number" id="update_pventa" placeholder="" class="form-control"/>
                </div>
                <!--
                <div class="form-group">
                    <label for="update_pcosto">$ Costo</label>
                    <input type="number" id="update_pcosto" placeholder="" class="form-control" />
                </div>
                -->

                
                <!--
                <div class="form-group">
                    <label for="update_vendedor">Vendedor</label>
                    <input type="text" id="update_vendedor" placeholder="" class="form-control"/>
                </div>
                -->
                <div class="form-group">
                <label for="update_vendedor">Vendedor</label>
                        <select id="update_vendedor" class="selectpicker" onchange="">
                          <optgroup label="Seleccionar vendedor">
                          <option><? echo $_SESSION['Nombre'] ?></option>
                          </optgroup>
                        </select>
                </div>
                <div class="form-group">
                <label for="update_tipopago">Medio de pago</label>
                        <select id="update_tipopago" class="selectpicker" onchange="">
                          <optgroup label="Seleccionar medio de pago">
                           <?php 
                $info = new CRUD();
                $paquetes = $info->ConsultaTipoPagosDisponibles();


                if (count($paquetes) > 0) {
                        foreach ($paquetes as $paquete) {
                                                                echo '<option>' . $paquete['nombre'] . '</option>';
                                                        }
                                          }



?>
                          </optgroup>
                        </select>
                </div>
</form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" onclick="UpdateUserDetails()" >Guardar</button>
                <input type="hidden" id="hidden_user_id">
                <input type="hidden" id="paqueteIDU">
                <input type="hidden" id="pagoID">
            </div>

        </div>
    </div>
</div>
<!-- // Modal -->


<!-- Modal - Update User details -->
<div class="modal fade" id="show_user_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Datos Completos</h4>
            </div>
            <div class="modal-body">




            <!--
        $("#update_first_name").val(user.first_name);
            $("#update_last_name").val(user.last_name);
            $("#update_email").val(user.email);
            $("#update_fnac").val(user.fnac);
            $("#update_tel").val(user.tel);
            $("#update_telseg").val(user.telseg);
            $("#update_paq").val(user.paq);
            $("#update_pventa").val(user.pventa);
            $("#update_pcosto").val(user.pcosto);
            $("#update_dni").val(user.dni);
            $("#update_vendedor").val(user.vendedor);

            -->

            <h4>Datos personales</h4>
            <form class="form-inline">
              <div class="form-group">
                <label for="show_first_name">Nombre</label>
                <input type="text" class="form-control" id="show_first_name" readonly>
              </div>
              <div class="form-group">
                <label for="show_last_name">Apellido</label>
                <input type="text" class="form-control" id="show_last_name" readonly>
              </div>
              <div class="form-group">
                <label for="show_fnac">F.Nac.</label>
                <input type="date" class="form-control" id="show_fnac" readonly>
              </div>
              <div class="form-group">
                <label for="show_dni">DNI</label>
                <input type="number" class="form-control" id="show_dni" readonly>
              </div>
              
              <div class="form-group">
                <label for="show_email">Email</label>
                <input type="email" class="form-control" id="show_email" readonly>
              </div>
              <div class="form-group">
                <label for="show_tel">Tel</label>
                <input type="number" class="form-control" id="show_tel" readonly>
              </div>
               <div class="form-group">
                <label for="show_familiar">Familiar</label>
                <input type="text" class="form-control" id="show_familiar" readonly>
              </div>
              <div class="form-group">
                <label for="show_telseg">Tel Seg</label>
                <input type="number" class="form-control" id="show_telseg" readonly>
              </div>

             
            </form>
           
            <h4>Datos del viaje</h4>

    <form class="form-inline">
                 <div class="form-group">
              <label class="sr-only" for="show_paq">Paquete</label>
              <div class="input-group">
                <div class="input-group-addon">Paquete</div>
                <input type="text" class="form-control" id="show_paq" readonly>
              </div>
            </div>


              <div class="form-group">
          <label class="sr-only" for="show_pcosto">COSTO</label>
          <div class="input-group">
            <div class="input-group-addon">$COSTO</div>
            <input type="number" class="form-control" id="show_pcosto" readonly>
          </div>
        </div>

        <div class="form-group">
          <label class="sr-only" for="show_pcosto">VENTA</label>
          <div class="input-group">
            <div class="input-group-addon">$VENTA</div>
            <input type="number" class="form-control" id="show_pventa" readonly>
          </div>
        </div>



        <div class="form-group">
          <label class="sr-only" for="show_dif">Vendedor</label>
          <div class="input-group">
            <div class="input-group-addon">Vendedor</div>
            <input type="text" class="form-control" id="show_vendedor" readonly>
          </div>
        </div>

          <div class="form-group">
            
              <label class="sr-only" for="show_tipopago">Pago</label>
              <div class="input-group">
                <div class="input-group-addon">Pago</div>
                <input type="text" class="form-control" id="show_tipopago" readonly>
              </div>
            </div>
              
             
            </form>


            <script>
              var clipboard = new Clipboard('#copiar');

                clipboard.on('success', function(e) {
                    e.clearSelection();
                    //alert('Copy to Clipboard Successfully');
                });

                clipboard.on('error', function(e) {
                    //alert('Something is wrong!');
                });

            </script>
            <div id="botoneslinks">
              <h4>Generar links de pago</h4>
              <span id="user_id" style="display: none"></span>
              <button type="button" class="btn btn-info btn-xs" disabled="disabled" onclick="LinkMP(id)">MercadoPago</button>
              <button type="button" class="btn btn-info btn-xs" onclick="LinkTP(id)">TodoPago</button>
                <div class="input-group input-group-sm" id="prueba2">
                  <span class="input-group-addon" id="basic-addon1"><button type="button" data-clipboard-action="copy" class="btn btn-info btn-xs glyphicon glyphicon-file" id="copiar" data-clipboard-target="#linkmp" style="display: none"></button></span>
                    <input type="text" class="form-control" id="linkmp" style="display: none" readonly>
                </div>

            </div>
              

                

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="cerrarver" data-dismiss="modal">Cerrar</button>
                <input type="hidden" id="hidden2_user_id">
                <input type="hidden" id="paqueteID">
                <input type="hidden" id="pagoID">
            </div>
        </div>
    </div>
</div>
<!-- // Modal -->



  </body>
</html>

<?php

}else {
    
    header("Location: ../../");
}

} else {
    header("Location: ../../");
}
?>