<?php

  session_start();
  //require '../Admin/pasajeros/ajax/lib.php';

include 'db.php';
include 'lib.php';



  if(isset($_SESSION['usuario'])){

    if($_SESSION['Tipo_usuario'] == "eviajes"){
      

?>



<!DOCTYPE html>
<html>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../EMT.ico">

    <title>Panel EMT</title>

    <script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>


<script type="text/javascript" src="js/script.js"></script>



    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>



    

  <style>

html, body{ font-family: 'Helvetica', sans-serif; background-color:#fafafa; color: #7A7A7A; }
.main {font-size:15px; text-align:center;  }




</style>
<script>
$(document).ready(function () {
    // READ records on page load
    $('#paxnotfound').hide();
});
</script>

  </head>
  <body>


<?php

$object = new CRUD();
$cant_sinemitir = count($object->SinEmitir());

?>

    <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Electronic Music Travel</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="index.php">General</a></li>
        <li><a href="emitidos.php">Emitidos</a></li>
        <li class="active"><a href="#">Buscador</a></li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Pasajeros <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="paquetes.php">Paquetes</a></li>
            <li class="divider"></li>
                        <?php 
                                      $info = new CRUD();
                                      $paquetes = $info->MenuPaquetes();


                                      if (count($paquetes) > 0) {
                                              foreach ($paquetes as $paquete) {
                                                    echo '<li><a href=paquetes/' . strtolower($paquete['nombre']) . '.php>' . $paquete['descripcion'] . '</a></li>';
                                                                              }
                                                                }
                                      


                      ?>
            <li class="divider"></li>
          </ul>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-user"></span><?php echo $_SESSION['Nombre']; echo '&nbsp'; echo $_SESSION['Apellido'];  ?></a></li>
        <li><a href="../logout.php"><span class="glyphicon glyphicon-log-in"></span> Cerrar sesión</a></li>
      </ul>
    </div>
  </div>
</nav>





<div class="container-fluid">


<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading text-center"><h3>Histórico pasajeros</h3></div>   
  </div>      
 </div>
 <div class="row">

<div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 ">
           <div class="form-group has-success">
                              <div class="input-group">
                                <div class="input-group-addon">Buscar</div>
                                <input type="text" class="search-query form-control" placeholder="Busqueda por DNI o apellido..." id="dnibuscar">
                                
                                <span class="input-group-btn">
                                              <button class="btn btn-success" type="button" onclick="Buscar()" id="btnbuscar">
                                                  <span class=" glyphicon glyphicon-search"></span>
                                              </button>
                                          </span>
                                
                              </div>
          </div>
</div>
<div class="col-md-2 col-md-offset-5">
  <p class="text-danger" id="paxnotfound"><b>Error!</b>Usuario no encontrado</p>
</div>

<div class="col-md-12"><center><h3 id="datospasajero"></h3></center></div>
<div class="col-md-6 text-right col-sm-6 text-center">
 <p id="first_name"></p>
 <p id="last_name"></p>
 <p id="dni"></p>
 <p id="fnac"></p>
</div>
<div class="col-md-6 col-sm-6 text-center">
 
 <p id=tel></p>
 <p id=telseg></p>
 <p id=familiar></p>
</div>


<div class="col-md-4 col-md-offset-4 col-sm-4">
  <div class="table-responsive">
  <div class="records_content"></div> 
</div>

</div>

    

</div>
</body>


</html>

<?php

}else {
  
  header("Location: ../Admin/index.php");
}

} else {
  header("Location: ../../");
}


?>