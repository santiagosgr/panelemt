<?php
session_start();
include_once("db.php");
$vendedor = $_SESSION['Nombre'];

if (isset($_POST["page"])) {
    $page_no = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);
    if(!is_numeric($page_no))
        die("Error fetching data! Invalid page number!!!");
} else {
    $page_no = 1;
}

// get record starting position
$start = (($page_no-1) * $row_limit);

$results = $pdo->prepare("SELECT * FROM users WHERE vendedor = :vendedor ORDER BY id desc LIMIT $start, $row_limit");
$results->bindParam("vendedor", $vendedor, PDO::PARAM_STR);
$results->execute();

while($row = $results->fetch(PDO::FETCH_ASSOC)) {
	$id = $row['id'];
    echo "<tr>" . 
    "<td>" . $row['first_name'] . "</td>" . 
    "<td>" . $row['last_name'] . "</td>" . 
    "<td>" . $row['tel'] . "</td>" . 
    "<td>" . $row['email'] . "</td>" . 
    "<td>" . $row['paq'] . "</td>" . 
    "</tr>";
}

?>