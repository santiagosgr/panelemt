<?php

require __DIR__ . '/db_connection.php';

class CRUD
{

    public $db;

    function __construct()
    {
        $this->db = DB();
    }

    function __destruct()
    {
        $this->db = null;
    }

    /*
     * Add new Record
     *
     * @param $first_name
     * @param $last_name
     * @param $email
     * @return $mixed
     * */

   




    //public function Create($first_name, $last_name, $email, $fnac, $tel, $telseg, $paq, $pventa, $pcosto, $dif, $dni, $vendedor, $link)
    public function Create($first_name, $last_name, $email, $fnac, $tel, $telseg, $paq, $pventa, $pcosto, $dif, $dni, $vendedor, $familiar, $tipopago, $eviajes)
    {
        $query = $this->db->prepare("INSERT INTO users(first_name, last_name, email, fnac, tel, telseg, paq, pventa, pcosto, dif, dni, vendedor, familiar, tipopago, eviajes) VALUES (:first_name,:last_name,:email,:fnac,:tel,:telseg,:paq,:pventa,:pcosto,:dif,:dni, :vendedor, :familiar, :tipopago, :eviajes)");
        $query->bindParam("first_name", $first_name, PDO::PARAM_STR);
        $query->bindParam("last_name", $last_name, PDO::PARAM_STR);
        $query->bindParam("email", $email, PDO::PARAM_STR);
        $query->bindParam('fnac', $fnac, PDO::PARAM_STR);
        $query->bindParam("tel", $tel, PDO::PARAM_INT);
        $query->bindParam("telseg", $telseg, PDO::PARAM_INT);
        $query->bindParam("paq", $paq, PDO::PARAM_STR);
        $query->bindParam("pventa", $pventa, PDO::PARAM_INT);
        $query->bindParam("pcosto", $pcosto, PDO::PARAM_INT);
        $query->bindParam("dif", $dif, PDO::PARAM_INT);
        $query->bindParam("dni", $dni, PDO::PARAM_INT);
        $query->bindParam("vendedor", $vendedor, PDO::PARAM_STR);
        $query->bindParam("familiar", $familiar, PDO::PARAM_STR);
        $query->bindParam("tipopago", $tipopago, PDO::PARAM_STR);
        $query->bindParam("eviajes", $eviajes, PDO::PARAM_STR);
        $query->execute();
        return $this->db->lastInsertId();
    }

    /*
     * Read all records
     *
     * @return $mixed
     * */
    public function Read()
    {
        $query = $this->db->prepare("SELECT * FROM users order by id desc LIMIT 10");
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    public function Buscar($param,$param2)
    {
    $query = $this->db->prepare("SELECT * FROM Pasajeros WHERE dni = :param OR last_name LIKE :param2 LIMIT 1"); 
        $term = '%' . $param2 . '%';
        $query->bindParam("param", $param, PDO::PARAM_STR);
        $query->bindValue("param2", $term, PDO::PARAM_STR);
        $query->execute();
        return json_encode($query->fetch(PDO::FETCH_ASSOC));
    }
        public function ConsultaPaquetesDisponibles()
    {
        $query = $this->db->prepare("SELECT * FROM NombrePaquetes order by id desc LIMIT 5");
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    public function ConsultaTipoPagosDisponibles()
    {
        $query = $this->db->prepare("SELECT * FROM TipoPagos order by id desc LIMIT 5");
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }


    public function BuscarHistorialPaquetes($param,$param2)
    {
    $query = $this->db->prepare("SELECT * FROM Paquetes WHERE idPasajero = (SELECT id FROM Pasajeros WHERE dni = :param OR last_name LIKE :term LIMIT 1)"); 
        $term = '%' . $param2 . '%';
        $query->bindParam("param", $param, PDO::PARAM_STR);
        $query->bindValue("term", $term, PDO::PARAM_STR);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    public function PasajerosPaquete($paq)
    {
        $query = $this->db->prepare("SELECT * FROM users WHERE paq = :paq");
        $query->bindParam("paq", $paq, PDO::PARAM_STR);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }


public function Rentabilidad()
    {
        $query = $this->db->prepare("SELECT SUM(dif) AS sum_dif FROM users WHERE pcosto != 0");
        $query->execute();
        $resultado = $query -> fetch(PDO::FETCH_ASSOC);
        $total = $resultado['sum_dif'];
        return $total;
    }

    public function RentabilidadPaquete($paq)
    {
        $query = $this->db->prepare("SELECT SUM(dif) AS sum_dif FROM users WHERE paq = :paq AND pcosto != 0");
        $query->bindParam("paq", $paq, PDO::PARAM_STR);
        $query->execute();
        $resultado = $query -> fetch(PDO::FETCH_ASSOC);
        $total = $resultado['sum_dif'];
        return $total;
    }

    public function RentabilidadVendedor($ven)
    {
        $query = $this->db->prepare("SELECT SUM(dif) AS sum_dif FROM users WHERE vendedor = :ven AND pcosto != 0");
        $query->bindParam("ven", $ven, PDO::PARAM_STR);
        $query->execute();
        $resultado = $query -> fetch(PDO::FETCH_ASSOC);
        $total = $resultado['sum_dif'];
        return $total;
    }

    public function RentabilidadPaqueteVendedor($paq,$ven)
    {
        $query = $this->db->prepare("SELECT SUM(dif) AS sum_dif FROM users WHERE paq = :paq AND vendedor = :ven AND pcosto != 0");
        $query->bindParam("paq", $paq, PDO::PARAM_STR);
        $query->bindParam("ven", $ven, PDO::PARAM_STR);
        $query->execute();
        $resultado = $query -> fetch(PDO::FETCH_ASSOC);
        $total = $resultado['sum_dif'];
        return $total;
    }



    public function SinEmitir()
    {
        $query = $this->db->prepare("SELECT * FROM users WHERE pcosto = 0");
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }



    public function EmitidosPorPaqueteVendedor($paq,$ven)
    {
        $query = $this->db->prepare("SELECT * FROM users WHERE paq = :paq AND vendedor = :ven");
        $query->bindParam("paq", $paq, PDO::PARAM_STR);
        $query->bindParam("ven", $ven, PDO::PARAM_STR);
        $query->execute();
        $cantidad = $query->rowCount();
        return $cantidad;
    }

    public function EmitidosPorPaquete($paq)
    {
        $query = $this->db->prepare("SELECT * FROM users WHERE paq = :paq AND pcosto != 0");
        $query->bindParam("paq", $paq, PDO::PARAM_STR);
        $query->execute();
        $cantidad = $query->rowCount();
        return $cantidad;
    }

     public function EmitidosPorVendedor($ven)
    {
        $query = $this->db->prepare("SELECT * FROM users WHERE vendedor = :ven AND pcosto != 0");
        $query->bindParam("ven", $ven, PDO::PARAM_STR);
        $query->execute();
        $cantidad = $query->rowCount();
        return $cantidad;
    }



    public function Emitidos()
    {
        $query = $this->db->prepare("SELECT * FROM users WHERE pcosto != 0");
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }
    public function PasajerosPorVendedor($vendedor)
    {
        $query = $this->db->prepare("SELECT * FROM users WHERE vendedor = :vendedor");
        $query->bindParam("vendedor", $vendedor, PDO::PARAM_STR);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    /*
     * Delete Record
     *
     * @param $user_id
     * */
    public function Delete($user_id)
    {
        $query = $this->db->prepare("DELETE FROM users WHERE id = :id");
        $query->bindParam("id", $user_id, PDO::PARAM_STR);
        $query->execute();
    }

    public function EliminarPaquetes($paq)
    {
        $query = $this->db->prepare("DELETE FROM users WHERE paq = :paq");
        $query->bindParam("paq", $paq, PDO::PARAM_STR);
        $query->execute();
    }
      public function EmitidosPorPerfilVendedor($vendedor)
    {
        $query = $this->db->prepare("SELECT * FROM users WHERE vendedor = :vendedor");
        $query->bindParam("vendedor", $vendedor, PDO::PARAM_STR);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    /*
     * Update Record
     *
     * @param $first_name
     * @param $last_name
     * @param $email
     * @return $mixed
     * */
    public function Update($first_name, $last_name, $email, $fnac, $tel, $telseg, $paq, $pventa, $pcosto, $dif, $dni, $vendedor, $familiar, $user_id)

    {
        $query = $this->db->prepare("UPDATE users SET first_name = :first_name, last_name = :last_name, email = :email, fnac = :fnac, tel = :tel, telseg = :telseg, paq = :paq, pventa = :pventa, pcosto = :pcosto, dif = :dif, dni = :dni, vendedor = :vendedor, familiar = :familiar WHERE id = :id");
        $query->bindParam("first_name", $first_name, PDO::PARAM_STR);
        $query->bindParam("last_name", $last_name, PDO::PARAM_STR);
        $query->bindParam("email", $email, PDO::PARAM_STR);
        //$naci = date("yyyy-MM-dd", strtotime($fnac));
        $query->bindParam('fnac', $fnac, PDO::PARAM_STR);
        //$handle->execute(array(":date"=>date("Y-m-d H:i:s", strtotime($date)), PDO::PARAM_STR));
        $query->bindParam("tel", $tel, PDO::PARAM_INT);
        $query->bindParam("telseg", $telseg, PDO::PARAM_INT);
        $query->bindParam("paq", $paq, PDO::PARAM_STR);
        $query->bindParam("pventa", $pventa, PDO::PARAM_INT);
        $query->bindParam("pcosto", $pcosto, PDO::PARAM_INT);
        $query->bindParam("dif", $dif, PDO::PARAM_INT);
        $query->bindParam("dni", $dni, PDO::PARAM_INT);
        $query->bindParam("vendedor", $vendedor, PDO::PARAM_STR);
        $query->bindParam("familiar", $familiar, PDO::PARAM_STR);
        $query->bindParam("id", $user_id, PDO::PARAM_STR);
        $query->execute();
    }

    /*
     * Get Details
     *
     * @param $user_id
     * */
    public function Details($user_id)
    {
        $query = $this->db->prepare("SELECT * FROM users WHERE id = :id");
        $query->bindParam("id", $user_id, PDO::PARAM_STR);
        $query->execute();
        return json_encode($query->fetch(PDO::FETCH_ASSOC));
    }



    public function Exportar($paq)
    {

        $query = $this->db->prepare("SELECT * FROM users WHERE paq = :paq");
                $query->bindParam("paq", $paq, PDO::PARAM_STR);
                $query->execute();
                $data = array();
                while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                    $data[] = $row;
                }
                return $data;


         
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=EMT_Pasajeros.csv');
        $output = fopen('php://output', 'w');
        fputcsv($output, array('ID', 'Nombre', 'Apellido', 'Email', 'F.Nacimiento', 'Telefono', 'Tel.Seguro', 'Paquete','$ VENTA', '$ EMISIÓN', '$ NETA', 'DNI', 'Vendedor', 'Link Comprobante'));
         
        if (count($users) > 0) {
            foreach ($users as $row) {
                fputcsv($data, $row);
            }
        }









    } // Fin exportar





}

?>