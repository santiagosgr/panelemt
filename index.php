<?php
   include("conexion.php");
   require 'password.php';
   session_start();


   class CRUD
{

    public $db;

    function __construct()
    {
        $this->db = DB();
    }

    function __destruct()
    {
        $this->db = null;
    }

}


$conexion = new CRUD();

if($_SERVER["HTTPS"] != "on")
{
    header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
    exit();
}

if($_SERVER["REQUEST_METHOD"] == "POST") {
$user = $_POST['emtu'];
$pw = $_POST['emtp'];

    //$hash_pw= hash('sha256', $pw); 
		$query = $conexion->db->prepare("SELECT * FROM CredencialesLogin WHERE usuario = :usuario");
	    $query->bindParam("usuario", $user, PDO::PARAM_STR);
	    $query->execute();
      $resultado = $query -> fetch(PDO::FETCH_ASSOC);;

      $hash_pw = $resultado['Password'];
      $nombre = $resultado['Nombre'];
      $Tipo_Usuario = $resultado['Tipo_Usuario'];
      $agencia = $resultado['agencia'];
      $apellido = $resultado['Apellido'];
      $usuario = $resultado['Usuario'];
      $imagen = $resultado['imagen'];


      
      if (password_verify($pw, $hash_pw)) {

        
        $_SESSION['usuario'] = $usuario;
        $_SESSION['Tipo_usuario'] = $Tipo_Usuario;
        $_SESSION['Nombre'] = $nombre;
        $_SESSION['Apellido'] = $apellido;
        $_SESSION['Agencia'] = $agencia;
        $_SESSION['imagen'] = $imagen;


          switch ($Tipo_Usuario) {
          case 'Admin':
              header("location: Admin/index.php");
              break;
          case 'eviajes':
              header("location: eviajes/index.php");
              break;
          case 'vendedor':
              header("location: vendedor/index.php");
              break;
          }
          



          

      }else {
         $error = "ERROR USUARIO INCORRECTO";
         echo $error;
         header("location: index.php?error=1");

         	
   



      }
   

}

 If(isset($_GET['error']) && $_GET['error'] == 1){ ?>
 <div class="col-sm-12 col-md-4 col-md-offset-4">
  <script>
$( document.body ).click(function() {
  $('.bg-danger').fadeIn( "slow" );
});
</script>
<center><p class="bg-danger"><b>Error</b>Usuario o contraseña incorrectos.</p></center>
      </div>
  <?php }

?>


<!DOCTYPE html>
<html>
<head>
	<title>Panel EMT</title>
	<meta charset="UTF-8">

  <link rel="icon" href="EMT.ico">


<script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>

  <!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">




<style>
.form-signin
{
    max-width: 330px;
    padding: 15px;
    margin: 0 auto;
}
.form-signin .form-signin-heading, .form-signin .checkbox
{
    margin-bottom: 10px;
}
.form-signin .checkbox
{
    font-weight: normal;
}
.form-signin .form-control
{
    position: relative;
    font-size: 16px;
    height: auto;
    padding: 10px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
.form-signin .form-control:focus
{
    z-index: 2;
}
.form-signin input[type="text"]
{
    margin-bottom: -1px;
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
}
.form-signin input[type="password"]
{
    margin-bottom: 10px;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
}
.account-wall
{
    margin-top: 40%;
    padding: 40px 0px 20px 0px;
    background-color: #f7f7f7;
    -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
}
.login-title
{
    color: #555;
    font-size: 18px;
    font-weight: 400;
    display: block;
}
.profile-img
{
    width: 96px;
    height: 96px;
    margin: 0 auto 10px;
    display: block;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    border-radius: 50%;
}
.need-help
{
    margin-top: 10px;
}
.new-account
{
    display: block;
    margin-top: 10px;
}
	
</style>



</head>
<body>



<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-4 col-md-offset-4">
            <!-- <h1 class="text-center login-title">Sign in to continue to Bootsnipp</h1> -->
            <div class="account-wall">
                <img class="profile-img" src="logo.png?sz=120">
                <form class="form-signin" method="post" id="formlg">
                <input type="text" name="emtu" class="form-control" placeholder="Usuario" pattern="[a-zA-Z]+" required autofocus>
                <input type="password" name="emtp" class="form-control" placeholder="Contraseña" pattern="[a-zA-Z0-9-]+" required>
                <button class="btn btn-lg btn-primary btn-block botonlg" type="submit">
                    Iniciar sesión</button>
                </form>
            </div>
            <h5 class="text-center"><b>Electronic Music Travel</b> - Panel v2.0</h5>
        </div>
    </div>
</div>


</body>
</html>