<?php
require 'pasajeros/ajax/lib.php';
	session_start();

	if(isset($_SESSION['usuario'])){

		if($_SESSION['Tipo_usuario'] == "Admin"){
			

?>



<!DOCTYPE html>
<html>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../EMT.ico">

    <title>Panel EMT</title>

    <script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>






    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


    

  <style>

html, body{ font-family: 'Helvetica', sans-serif; background-color:#fafafa; color: #7A7A7A; }
.main {font-size:15px; text-align:center;  }




</style>


  </head>
  <body>


<?php

$object = new CRUD();




//Cantidad de paquetes por vendedor.
$cant_mariano = $object->EmitidosPorVendedor('Mariano');
$cant_hernan = $object->EmitidosPorVendedor('Hernan');
$cant_santiago = $object->EmitidosPorVendedor('Santiago');


// Cantidades generales
$cant_emitidos = count($object->Emitidos());
$cant_sinemitir = count($object->SinEmitir());

// Rentabilidad total
//Sumatoria de diferencias de paquetes emitidos.
$rentabilidadtotal = $object->Rentabilidad();
if ($cant_emitidos!=0) {
$rentabilidadProm = round($rentabilidadtotal / $cant_emitidos, 2); 
} else {
$rentabilidadProm = 0;
}






?>

    <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Electronic Music Travel</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Estadísticas</a></li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Pasajeros <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="pasajeros/index.php">Últimos pasajeros</a></li>
            <li class="divider"></li>
            <!--
            <li><a href="pasajeros/hernan.php">Hernan @ Warung</a></li> -->
            <?php 
                                      $info = new CRUD();
                                      $paquetes = $info->MenuPaquetes();


                                      if (count($paquetes) > 0) {
                                              foreach ($paquetes as $paquete) {
                                                    echo '<li><a href=pasajeros/' . strtolower($paquete['nombre']) . '.php>' . $paquete['descripcion'] . '</a></li>';
                                                                              }
                                                                }
                                      


                      ?>
          </ul>
        </li>
        <li><a href="pagos/pagos.php">Pagos</a></li>
        <li><a href="historico/index.php">Histórico</a></li>
        <li><a href="paquetes/paquetes.php">Paquetes</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="perfil/index.php"><span class="glyphicon glyphicon-user"></span><?php echo $_SESSION['Nombre']; echo '&nbsp'; echo $_SESSION['Apellido'];  ?></a></li>
        <li><a href="../logout.php"><span class="glyphicon glyphicon-log-in"></span> Cerrar sesión</a></li>
      </ul>
    </div>
  </div>
</nav>

<!--
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-1 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active" ><a href="/PanelEMT/Main_app/Admin">General <span class="pull-right glyphicon glyphicon-info-sign"></span></a></li>
            <li><a href="/PanelEMT/Main_app/Admin/pasajeros/index.php">Pasajeros <span class="pull-right hidden-xs showopacity glyphicon glyphicon-user badge"><?php echo $cant_sinemitir ?></span></a></li>
            <li><a href="exportar.php">Exportar/Borrar<span class="pull-right glyphicon glyphicon-export"></span></a></li>

          </ul>
          
        </div>
      </div>
      -->




<div class="container-fluid">


<div class="row">
<div class="col-sm-12 col-sm-offset- col-md-12-offset-2 main">


<?php 
if ($cant_sinemitir > 0) {

  ?>


<div class="alert alert-danger alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Hay <?php echo $cant_sinemitir ?> paquetes sin emitir.</strong>
  </div>

<?php 
}
?>


<ul class="list-group">

  <li class="list-group-item list-group-item-danger">
    <span class="badge"><?php echo $cant_sinemitir ?></span>
    Sin emitir
  </li>

  <li class="list-group-item list-group-item-success">
    <span class="badge"><?php echo $cant_emitidos ?></span>
    Emitidos
  </li>



<?php 
                                      $info = new CRUD();
                                      $paquetes = $info->ConsultaPaquetesDisponibles();


                                      if (count($paquetes) > 0) {
                                              foreach ($paquetes as $paquete) {
                                                    echo '<li class="list-group-item">';
                                                    echo '<span class="badge">' . $info->CantidadPorPaquete($paquete['nombre']) . '</span>' . $paquete['descripcion'];
                                                    echo '</li>';
                                                                              }
                                                                }
                                      


                      ?>

</ul>

<div class="panel panel-info">
  <!-- Default panel contents -->
  <div class="panel-heading">Rentabilidades</div>
  <!-- Table -->
  <table class="table table-condensed">

    <tbody>
      <tr class="active">
        <td><b>Paquete</b></td>
        <td><b>Rent.</b></td>
        <td><b>Rent. prom.</b></td>
      </tr>
      <tr class="active">
        <td>Total</td>
        <td>$<?php echo $rentabilidadtotal ?></td>
        <td>$<?php echo $rentabilidadProm ?></td>
      </tr>
      
        <?php 
                                      $info = new CRUD();
                                      $paquetes = $info->ConsultaPaquetesDisponibles();


                                      if (count($paquetes) > 0) {
                                              foreach ($paquetes as $paquete) {
                                                    echo '<tr>';
                                                    echo '<td>' . $paquete['descripcion'] . '</td>';
                                                    echo '<td> $' . $info->RentabilidadPaquete($paquete['nombre']) . '</td>';
                                                    echo '<td> $' . $info->RentabilidadPaquetePromedio($paquete['nombre']) . '</td>';
      
                                                    echo '</tr>';
                                                                              }
                                                                }
                                      


                      ?>
    </tbody>
  </table>
</div>
<center>
<div class="panel panel-warning">
  <!-- Default panel contents -->
  <div class="panel-heading">Vendedores</div>
  <!-- Table -->
  <table class="table table-condensed">

    <tbody>
      <tr class="active">
        <td><b>Vendedor</b></td>
        <td><b>Paq. vendidos</b></td>
    </tr>

      <?php 
                                      $info = new CRUD();
                                      $vendedores = $info->Vendedores();


                                      if (count($vendedores) > 0) {
                                              foreach ($vendedores as $vendedor) {
                                                    echo '<tr>';
                                                    echo '<td>' . $vendedor['Nombre'] . '</td>';
                                                    echo '<td>' . $info->EmitidosPorVendedor($vendedor['Nombre']) . '</td>';
      
                                                    echo '</tr>';
                                                                              }
                                                                }
                                      


                      ?>
    </tbody>
  </table>
</div>
</center>


</div> <!-- col -->
</div> <!-- row -->
</div> <!-- container -->

  </body>
</html>

<?php

}else {
	
	 header("Location: ../");
}

} else {
	header("Location: ../");
}


?>