<?php
require '../pasajeros/ajax/lib.php';
	session_start();

	if(isset($_SESSION['usuario'])){

		if($_SESSION['Tipo_usuario'] == "Admin"){
			

?>



<!DOCTYPE html>
<html>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../EMT.ico">

    <title>Panel EMT</title>

    <script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>


  <script type="text/javascript">



    
function AgregarGasto() {

var tipo = $('#tipo').val();
var monto = $('#monto').val();
var descripcion = $('#descripcion').val();
var paq = $('#paq').val();



      	$.post("../pasajeros/agregar_gastos.php", {
      		paq:paq,
      		descripcion:descripcion,
      		monto:monto,
            tipo:tipo
            
           
        }, function (data, status) {
          location.reload();

        

          });

              



}

function ExportarPDF(parametro) {



window.open("../pdf/pdf.php?parametro="+parametro, '_blank');

}




function ExportarExcel(parametro)
{

      window.open("export.php?parametro="+parametro, '_blank');

}



$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});





  </script>






    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


    

  <style>

html, body{ font-family: 'Helvetica', sans-serif; background-color:#fafafa; color: #7A7A7A; }
.main {font-size:15px; text-align:center;  }




</style>


  </head>
  <body>




    <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Electronic Music Travel</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="../index.php">Estadísticas</a></li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Pasajeros <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="../pasajeros/index.php">Últimos pasajeros</a></li>
            <li class="divider"></li>
            <!--
            <li><a href="pasajeros/hernan.php">Hernan @ Warung</a></li> -->
            <?php 
                                      $info = new CRUD();
                                      $paquetes = $info->MenuPaquetes();


                                      if (count($paquetes) > 0) {
                                              foreach ($paquetes as $paquete) {
                                                    echo '<li><a href=../pasajeros/' . strtolower($paquete['nombre']) . '.php>' . $paquete['descripcion'] . '</a></li>';
                                                                              }
                                                                }
                                      


                      ?>
          </ul>
        </li>
        <li><a href="../pagos/pagos.php">Pagos</a></li>
        <li><a href="../historico/index.php">Histórico</a></li>
        <li><a href="../paquetes/paquetes.php">Paquetes</a></li>
        <li class="active"><a href="#">Gastos</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="../perfil/index.php"><span class="glyphicon glyphicon-user"></span><?php echo $_SESSION['Nombre']; echo '&nbsp'; echo $_SESSION['Apellido'];  ?></a></li>
        <li><a href="../../logout.php"><span class="glyphicon glyphicon-log-in"></span> Cerrar sesión</a></li>
      </ul>
    </div>
  </div>
</nav>




<!-- Button trigger modal -->
<center><button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#myModal">
  Agregar gastos
</button></center>

<table class="table table-bordered table-striped table-condensed" width="100%">
            <tr>
              <th>Tipo</th>
              <th>Descripciòn</th>
              <th>Monto</th>
              <th>Paquete</th>
            </tr>

<?php

$object = new CRUD();

$gastos = $object->ConsultarGastos();

foreach ($gastos as $gasto) {
  echo '<tr>';
  echo '<td>' . $gasto['tipo'] . '</td>';
  echo '<td>' . $gasto['descripcion'] . '</td>';
  echo '<td>' . $gasto['monto'] . '</td>';
  echo '<td>' . $gasto['nombre'] . '</td>';
  echo '</tr>';
  
}




?>
</table>





<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Agregar gastos</h4>
      </div>
      <div class="modal-body">



      	<form id="agregargasto" class="form-inline">
        	<div class="form-group required">
              <label for="tipo">Tipo</label>
              <select id="tipo" class="selectpicker">
              <optgroup label="Seleccionar el tipo de gasto">
              <?php 
                $info = new CRUD();
                $gastos = $info->ConsultarTipoGastosDisponibles();


                if (count($gastos) > 0) {
                        foreach ($gastos as $gasto) {
                                                                echo '<option>'. $gasto['descripcion'] . '</option>';
                                                        }
                                          }



?>

              </optgroup>
            </select>
          </div><br>
        
        	<div class="form-group required">
              <label for="paq">Paquete</label>
              <select id="paq" class="selectpicker">
              <optgroup label="Adjudicar el gasto de coordinación a un paquete">
              <?php 
                $info = new CRUD();
                $paquetes = $info->ConsultaPaquetesDisponibles();


                if (count($paquetes) > 0) {
                        foreach ($paquetes as $paquete) {
                                                                echo '<option value='. $paquete['nombre'] . '>' . $paquete['descripcion'] . '</option>';
                                                        }
                                          }



?>

              </optgroup>
            </select>
          </div>

                <div class="form-group required">
                    <label for="descripcion">Descripción</label>
                    <input type="text" id="descripcion" placeholder="" required />
                </div>

                <div class="form-group required">
                    <label for="monto">Monto</label>
                    <input type="number" id="monto" placeholder="" required="" />
                </div>
            

         <!-- <input type="checkbox" name="subpaquete" id="subpaquete" value="si"> ¿Es subpaquete? -->



        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-success" onclick="AgregarGasto()">Agregar</button>
      </div>
    </form>
    </div>
  </div>
</div>





  </body>
</html>

<?php

}else {
	
	 header("Location: ../");
}

} else {
	header("Location: ../");
}


?>