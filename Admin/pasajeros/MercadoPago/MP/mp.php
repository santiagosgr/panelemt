<?php
require_once ('mercadopago.php');


   $pventa = $_POST['pventa'];
   $paq = $_POST['paq'];

   switch ($paq) {
    case 'Hernan17':
        $nombre = "Paquete de viaje: Hernan Cattaneo @ Warung Beach Club";
        break;
    case 'Miami18':
        $nombre = "Paquete de viaje: Miami Music Week + Ultra Miami 2018";
        break;
    case 'Ultra17':
        $nombre = "Paquete de viaje: Ultra Rio 2017";
        break;
	}





$mp = new MP ("3227750870270011", "QvO5Z5SbBQk55RvilyOaigrEMGQbPBZO");

$preference_data = array(
	"items" => array(
		array(
			"id" => "Code",
			"title" => $nombre,
			"currency_id" => "ARS",
			"picture_url" =>"https://www.mercadopago.com/org-img/MP3/home/logomp3.gif",
			"description" => "Aéreo, hotel y traslados.",
			"category_id" => "Category",
			"quantity" => 1,
			"unit_price" => floatval($pventa)
		)
	),
	"payer" => array(
		"name" => "user-name",
		"surname" => "user-surname",
		"email" => "user@email.com",
		"date_created" => "2014-07-28T09:50:37.521-04:00",
		"phone" => array(
			"area_code" => "11",
			"number" => "4444-4444"
		),
		"identification" => array(
			"type" => "DNI",
			"number" => "12345678"
		),
		"address" => array(
			"street_name" => "Street",
			"street_number" => 123,
			"zip_code" => "1430"
		)
	),
	"back_urls" => array(
		"success" => "https://www.success.com",
		"failure" => "http://www.failure.com",
		"pending" => "http://www.pending.com"
	),
	"auto_return" => "approved",
	"payment_methods" => array(
		"excluded_payment_methods" => array(
			array(
				"id" => "amex",
			)
		),
		"excluded_payment_types" => array(
			array(
				"id" => "ticket"
			)
		),
		"installments" => 24,
		"default_payment_method_id" => null,
		"default_installments" => null,
	),
	"shipments" => array(
		"receiver_address" => array(
			"zip_code" => "1430",
			"street_number"=> 123,
			"street_name"=> "Street",
			"floor"=> 4,
			"apartment"=> "C"
		)
	),
	"notification_url" => "https://www.your-site.com/ipn",
	"external_reference" => "Reference_1234",
	"expires" => false,
	"expiration_date_from" => null,
	"expiration_date_to" => null
);
$preference = $mp->create_preference($preference_data);


$data = $preference["response"]["init_point"];
echo $data;
?>
