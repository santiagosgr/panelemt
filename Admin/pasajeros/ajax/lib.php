<?php

require __DIR__ . '/db_connection.php';

class CRUD
{

    public $db;

    function __construct()
    {
        $this->db = DB();
    }

    function __destruct()
    {
        $this->db = null;
    }

    /*
     * Add new Record
     *
     * @param $first_name
     * @param $last_name
     * @param $email
     * @return $mixed
     * */



public function ConsultarTarjetas()
    {
        $query = $this->db->prepare("SELECT * FROM Tarjetas order by id asc");
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }


public function ConsultarAgencias()
    {
        $query = $this->db->prepare("SELECT * FROM Agencias order by id asc");
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

public function ConsultarVendedores()
    {
        $query = $this->db->prepare("SELECT Nombre FROM CredencialesLogin WHERE Tipo_usuario = 'Admin' OR Tipo_Usuario = 'vendedor' order by id asc");
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

   

public function Create($first_name, $last_name, $email, $fnac, $tel, $telseg, $paq, $pventa, $pcosto, $dif, $dni, $vendedor, $familiar, $tipopago, $eviajes)
{
       

  
$this->db->beginTransaction();

$pasajeroExistente = False;
$query = $this->db->prepare("SELECT id FROM Pasajeros WHERE dni = :dni");
$query->bindParam("dni", $dni, PDO::PARAM_INT);
$query->execute();
$cantidad = $query->rowCount();
$resultado = $query->fetch(PDO::FETCH_OBJ);
$PasajeroID = $resultado->id;



if ($cantidad == 0) {
$query = $this->db->prepare("INSERT INTO Pasajeros(first_name, last_name,email,fnac,tel,telseg,familiar, dni) VALUES (:first_name,:last_name,:email,:fnac,:tel,:telseg,:familiar,:dni)");
$query->bindParam("first_name", strtoupper($first_name), PDO::PARAM_STR);
$query->bindParam("last_name", strtoupper($last_name), PDO::PARAM_STR);
$query->bindParam("email", strtoupper($email), PDO::PARAM_STR);
$query->bindParam('fnac', $fnac, PDO::PARAM_STR);
$query->bindParam("tel", $tel, PDO::PARAM_INT);
$query->bindParam("telseg", $telseg, PDO::PARAM_INT);
$query->bindParam("familiar", strtoupper($familiar), PDO::PARAM_STR);
$query->bindParam("dni", $dni, PDO::PARAM_INT);
$query->execute();
$PasajeroID = $this->db->lastInsertId();
}
//Inserto Tabla Pagos
 $query = $this->db->prepare("INSERT INTO Pagos(tipopago) VALUES (:tipopago)");
 $query->bindParam("tipopago", $tipopago, PDO::PARAM_STR);
 $query->execute();
 $pagoID = $this->db->lastInsertId();
 //Inserto Tabla Paquetes
$query = $this->db->prepare("INSERT INTO Paquetes(paq,pventa,pcosto,dif,eViajes,vendedor,idPagos,idPasajero,fecha) VALUES (:paq,:pventa,:pcosto,:dif,:eviajes,:vendedor,:idPagos,:idPasajero,CURDATE())");    
$query->bindParam("paq", $paq, PDO::PARAM_STR);
$query->bindParam("pventa", $pventa, PDO::PARAM_INT);
$query->bindParam("pcosto", $pcosto, PDO::PARAM_INT);
$query->bindParam("dif", $dif, PDO::PARAM_INT);
$query->bindParam("eviajes", $eviajes, PDO::PARAM_STR);
$query->bindParam("vendedor", $vendedor, PDO::PARAM_STR);
$query->bindParam("idPagos", $pagoID, PDO::PARAM_INT);
$query->bindParam("idPasajero", $PasajeroID, PDO::PARAM_INT);
$query->execute();
$PaqueteID = $this->db->lastInsertId();
$this->db->commit();

return $PasajeroID;
}

    /*
     * Read all records
     *
     * @return $mixed
     * */
    public function Read()
    {
         $query = $this->db->prepare("SELECT Pasajeros.id,Pasajeros.first_name, Pasajeros.last_name, Paquetes.paq, Paquetes.pventa,Paquetes.id AS paqID, Paquetes.pcosto, Paquetes.dif, Paquetes.vendedor FROM Pasajeros INNER JOIN Paquetes ON Paquetes.idPasajero = Pasajeros.id ORDER by Paquetes.id desc LIMIT 10");
         //$query = $this->db->prepare("SELECT * FROM ConsultaCompleta order by id  LIMIT 10");
        /* $ $query = $this->db->prepare("SELECT Pasajeros.first_name, Pasajeros.last_name, Paquetes.nombre, Paquetes.pventa, Paquetes.pcosto, Paquetes.dif, Paquetes.vendedor FROM Pasajeros INNER JOIN Paquetes ON Pasajeros.idPasajeros = Paquetes.idPaquete ORDER by Pasajeros.idPasajeros desc LIMIT 10"); */
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }
    public function PaquetePorVendedor($vendedor)
    {
         $query = $this->db->prepare("SELECT Pasajeros.id,Pasajeros.first_name, Pasajeros.last_name, Paquetes.paq, Paquetes.pventa,Paquetes.id AS paqID, Paquetes.pcosto, Paquetes.dif, Paquetes.vendedor FROM Pasajeros INNER JOIN Paquetes ON Paquetes.idPasajero = Pasajeros.id WHERE Paquetes.vendedor = :vendedor ORDER by Paquetes.id desc LIMIT 10");
         $query->bindParam("vendedor", $vendedor, PDO::PARAM_STR);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    public function PaqueteAExportar($paq)
    {
         $query = $this->db->prepare("SELECT last_name,first_name,fnac,tel,telseg,familiar FROM Pasajeros WHERE id IN (SELECT idPasajero FROM Paquetes WHERE paq=:paq) ORDER BY last_name ASC");
         $query->bindParam("paq", $paq, PDO::PARAM_STR);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }



    public function ConsultaPaquetesDisponibles()
    {
        $query = $this->db->prepare("SELECT * FROM NombrePaquetes order by id desc LIMIT 4");
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    public function ConsultarTipoGastosDisponibles()
    {
        $query = $this->db->prepare("SELECT descripcion FROM TipoGastos order by id desc LIMIT 4");
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    public function ConsultarGastos()
    {
        $query = $this->db->prepare("SELECT tipo,Gastos.descripcion,monto,NombrePaquetes.nombre FROM Gastos INNER JOIN NombrePaquetes ON NombrePaquetes.id=Gastos.paqueteID LIMIT 10");
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    public function ConsultaPaquetesDisponiblesTodos()
    {
        $query = $this->db->prepare("SELECT * FROM NombrePaquetes order by id desc");
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }



    public function MenuPaquetes()
    {
        $query = $this->db->prepare("SELECT * FROM NombrePaquetes WHERE nombre  NOT LIKE '%-%' order by id desc LIMIT 3");
        //$term = '%' . $param2 . '%';
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    public function ConsultaTipoPagosDisponibles()
    {
        $query = $this->db->prepare("SELECT * FROM TipoPagos order by id desc LIMIT 5");
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }





    public function PasajerosPaquete($paq,$paq2)

    {
        $query = $this->db->prepare("SELECT Pasajeros.id,Pasajeros.first_name, Pasajeros.last_name, Paquetes.paq, Paquetes.pventa,Paquetes.id AS paqID, Paquetes.pcosto, Paquetes.dif, Paquetes.vendedor FROM Pasajeros INNER JOIN Paquetes ON Paquetes.idPasajero = Pasajeros.id WHERE Paquetes.paq = :paq OR Paquetes.paq LIKE :paq2 ORDER by Paquetes.id DESC");
        $term = $paq2 . '%';
        $query->bindParam("paq", $paq, PDO::PARAM_STR);
        $query->bindParam("paq2", $term, PDO::PARAM_STR);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }


public function Rentabilidad()
    {
        $query = $this->db->prepare("SELECT SUM(dif) AS sum_dif FROM Paquetes WHERE pcosto != 0");
        $query->execute();
        $resultado = $query -> fetch(PDO::FETCH_ASSOC);
        $total = $resultado['sum_dif'];
        return $total;
    }

    public function RentabilidadPaquete($paq)
    {
        $query = $this->db->prepare("CALL RentabilidadPaquete(?);");
        $query->bindParam(1, $paq, PDO::PARAM_STR);
        $query->execute();
        $resultado = $query -> fetch(PDO::FETCH_ASSOC);
        $total = $resultado['rentabilidad'];

        return $total;
    }

    public function RentabilidadPaquetePromedio($paq)
    {
        $query = $this->db->prepare("CALL RentabilidadPaquetePromedio(?);");
        $query->bindParam(1, $paq, PDO::PARAM_STR);
        $query->execute();
        $resultado = $query -> fetch(PDO::FETCH_ASSOC);
        $total = $resultado['resultado'];
        return $total;
    }

    public function Vendedores()
    {
        $query = $this->db->prepare("SELECT * FROM CredencialesLogin WHERE Tipo_usuario != 'eviajes' ORDER BY Nombre");
        $query->bindParam(1, $paq, PDO::PARAM_STR);
        $query->execute();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    public function VendedoresDeAgencia($agencia)
    {
        $query = $this->db->prepare("SELECT * FROM CredencialesLogin WHERE Tipo_usuario = 'vendedor' AND  agencia = :eviajes");
        $query->bindParam("eviajes", $agencia, PDO::PARAM_STR);
        $query->execute();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    public function RentabilidadVendedor($ven)
    {
        $query = $this->db->prepare("SELECT SUM(dif) AS sum_dif FROM Paquetes WHERE vendedor = :ven AND pcosto != 0");
        $query->bindParam("ven", $ven, PDO::PARAM_STR);
        $query->execute();
        $resultado = $query -> fetch(PDO::FETCH_ASSOC);
        $total = $resultado['sum_dif'];
        return $total;
    }

    public function RentabilidadPaqueteVendedor($paq,$ven)
    {
        $query = $this->db->prepare("SELECT SUM(dif) AS sum_dif FROM Paquetes WHERE paq = :paq AND vendedor = :ven AND pcosto != 0");
        $query->bindParam("paq", $paq, PDO::PARAM_STR);
        $query->bindParam("ven", $ven, PDO::PARAM_STR);
        $query->execute();
        $resultado = $query -> fetch(PDO::FETCH_ASSOC);
        $total = $resultado['sum_dif'];
        return $total;
    }


    public function ConsultarCuotasPaquete($paq)
    {
        $query = $this->db->prepare("SELECT cuotas FROM NombrePaquetes WHERE nombre = :paq");
        $query->bindParam("paq", $paq, PDO::PARAM_STR);
        $query->execute();
        $resultado = $query -> fetch(PDO::FETCH_ASSOC);
        $total = $resultado['cuotas'];
        return $total;
    }

    public function AgregarPaquete($paq,$descripcion,$cuotas,$fecha)

    {
        $paq2 = ucfirst($descripcion);
        $query = $this->db->prepare("INSERT INTO NombrePaquetes(nombre,descripcion,cuotas,fecha) VALUES(:paq,:descripcion,:cuotas,:fecha)");
        $query->bindParam("paq", $paq, PDO::PARAM_STR);
        $query->bindParam("descripcion",  $paq2, PDO::PARAM_STR);
        $query->bindParam("cuotas",  $cuotas, PDO::PARAM_INT);
        $query->bindParam("fecha",  $fecha, PDO::PARAM_STR);
        $query->execute();

    }

    public function AgregarGastos($paq,$descripcion,$monto,$tipo)

    {

        //Busca ID de paquete
        $query = $this->db->prepare("SELECT id FROM NombrePaquetes WHERE nombre = :paq");
        $query->bindParam("paq", $paq, PDO::PARAM_STR);
        $query->execute();
        $resultado = $query -> fetch(PDO::FETCH_ASSOC);
        $paqID = $resultado['id'];




        $query = $this->db->prepare("INSERT INTO Gastos(tipo,descripcion,monto,paqueteID,fecha) VALUES(:tipo,:descripcion,:monto,:paqID,CURDATE())");
        $query->bindParam("tipo", $tipo, PDO::PARAM_STR);
        $query->bindParam("descripcion",  $descripcion, PDO::PARAM_STR);
        $query->bindParam("monto",  $monto, PDO::PARAM_INT);
        $query->bindParam("paqID",  $paqID, PDO::PARAM_INT);
        $query->execute();

    }


    public function GenerarArchivoPaquete($paq,$descripcion) {

        fopen(strtolower($paq) . '.php',"w+");
        fopen('../../eviajes/paquetes/' . strtolower($paq) . '.php',"w+");
        $fichero = 'plantilla.php';
        $fichero_eviajes = 'plantilla_eviajes.php';
        $nuevo_fichero = strtolower($paq) . '.php';
        $nuevo_fichero_eviajes = '../../eviajes/paquetes/' . strtolower($paq) . '.php';

        if (!copy($fichero, $nuevo_fichero)) {
            echo "Error al generar $fichero...\n";
        }
        if (!copy($fichero_eviajes, $nuevo_fichero_eviajes)) {
            echo "Error al generar $fichero...\n";
        }




    }







    public function SinEmitir()
    {
        $query = $this->db->prepare("SELECT * FROM Paquetes WHERE pcosto = 0");
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }
     public function EmitidosPorPerfilVendedor($vendedor)
    {
        $query = $this->db->prepare("SELECT * FROM Paquetes WHERE vendedor = :vendedor");
        $query->bindParam("vendedor", $vendedor, PDO::PARAM_STR);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }



    public function EmitidosPorPaqueteVendedor($paq,$ven)
    {
        $query = $this->db->prepare("SELECT * FROM Paquetes WHERE paq = :paq AND vendedor = :ven");
        $query->bindParam("paq", $paq, PDO::PARAM_STR);
        $query->bindParam("ven", $ven, PDO::PARAM_STR);
        $query->execute();
        $cantidad = $query->rowCount();
        return $cantidad;
    }

    public function EmitidosPorPaquete($paq)
    {
        $query = $this->db->prepare("SELECT * FROM Paquetes WHERE paq = :paq AND pcosto != 0");
        $query->bindParam("paq", $paq, PDO::PARAM_STR);
        $query->execute();
        $cantidad = $query->rowCount();
        return $cantidad;
    }

    public function CantidadPorPaquete($paq)
    {
        $query = $this->db->prepare("SELECT * FROM Paquetes WHERE paq = :paq");
        $query->bindParam("paq", $paq, PDO::PARAM_STR);
        $query->execute();
        $cantidad = $query->rowCount();
        return $cantidad;
    }

     public function EmitidosPorVendedor($ven)
    {
        $query = $this->db->prepare("SELECT * FROM Paquetes WHERE vendedor = :ven AND pcosto != 0");
        $query->bindParam("ven", $ven, PDO::PARAM_STR);
        $query->execute();
        $cantidad = $query->rowCount();
        return $cantidad;
    }

    public function Emitidos()
    {
        $query = $this->db->prepare("SELECT * FROM Paquetes WHERE pcosto != 0");
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }


    public function Delete($paq_id)
    {

    	//

    	$this->db->beginTransaction();

/*
    	$query = $this->db->prepare("DELETE FROM Paquetes WHERE id = :id");
        $query->bindParam("id", $paq_id, PDO::PARAM_STR);
        $query->execute();
*/
    	$query = $this->db->prepare("DELETE FROM Pagos WHERE id = (SELECT idPagos FROM Paquetes WHERE id = :id)");
    	$query->bindParam("id", $paq_id, PDO::PARAM_STR);
        $query->execute();
        $this->db->commit();
        




    }

    public function EliminarPaquetes($paq)
    {
        $query = $this->db->prepare("DELETE FROM users WHERE paq = :paq");
        $query->bindParam("paq", $paq, PDO::PARAM_STR);
        $query->execute();
    }

    public function OperationID($opid, $idPaq)
    {
        $query = $this->db->prepare("UPDATE Pagos SET opid = :opid WHERE id=(SELECT idPagos FROM Paquetes WHERE id = :idPaq)");
        $query->bindParam("opid", $opid, PDO::PARAM_INT);
        $query->bindParam("idPaq", $idPaq, PDO::PARAM_STR);
        $query->execute();
    }


    public function Update($first_name, $last_name, $email, $fnac, $tel, $telseg, $paq, $pventa, $dni, $vendedor, $familiar, $tipopago, $user_id, $paqueteID, $pagoID)

    {
        $nombre = strtoupper($first_name);
        $apellido = strtoupper($last_name);
        $mail = strtoupper($email);
        $familiar2 = strtoupper($familiar);

         //$query = $this->db->prepare("UPDATE users SET first_name = :first_name, last_name = :last_name, email = :email, fnac = :fnac, tel = :tel, telseg = :telseg, paq = :paq, pventa = :pventa, dni = :dni, vendedor = :vendedor, familiar = :familiar, tipopago = :tipopago WHERE id = :id");

        $query = $this->db->prepare("UPDATE Pasajeros SET first_name = :first_name, last_name = :last_name, email = :email, fnac = :fnac, tel = :tel, telseg = :telseg, dni = :dni, familiar = :familiar WHERE id = :id");
        $query->bindParam("first_name", $nombre, PDO::PARAM_STR);
        $query->bindParam("last_name", $apellido, PDO::PARAM_STR);
        $query->bindParam("email", $mail, PDO::PARAM_STR);
        $query->bindParam('fnac', $fnac, PDO::PARAM_STR);
        $query->bindParam("tel", $tel, PDO::PARAM_INT);
        $query->bindParam("telseg", $telseg, PDO::PARAM_INT);
        $query->bindParam("dni", $dni, PDO::PARAM_INT);
        $query->bindParam("familiar", $familiar2, PDO::PARAM_STR);
        $query->bindParam("id", $user_id, PDO::PARAM_STR);
        $query->execute();
        $query = $this->db->prepare("UPDATE Pagos SET tipopago = :tipopago WHERE id = :id2");
        $query->bindParam("tipopago", $tipopago, PDO::PARAM_STR);
        $query->bindParam("id2", $pagoID, PDO::PARAM_STR);
        $query->execute();
        $query = $this->db->prepare("UPDATE Paquetes SET paq = :paq, pventa = :pventa, vendedor = :vendedor WHERE id = :id3");
        $query->bindParam("paq", $paq, PDO::PARAM_STR);
        $query->bindParam("pventa", $pventa, PDO::PARAM_INT);
        $query->bindParam("vendedor", $vendedor, PDO::PARAM_STR);
        $query->bindParam("id3", $paqueteID, PDO::PARAM_STR);
        $query->execute();


    }

    public function OpIDPorUserID($id,$paqID)
    {
        $query = $this->db->prepare("SELECT opid FROM Pagos WHERE id = (SELECT idPagos FROM Paquetes WHERE idPasajero = :idPasajero AND id = :paqID)");
        $query->bindParam("idPasajero", $id, PDO::PARAM_STR);
        $query->bindParam("paqID", $paqID, PDO::PARAM_STR);
        $query->execute();
        $resultado = $query -> fetch(PDO::FETCH_ASSOC);;
        $opid = $resultado['opid'];
        return $opid;
    }

    /*
     * Get Details
     *
     * @param $user_id
     * */

    public function FechaPaquete($fecha1,$fecha2)
    {
        //$query = $this->db->prepare("SELECT * FROM users WHERE id = :id"); 
        //$query = $this->db->prepare("SELECT * FROM ConsultaCompleta WHERE id = :id");
        $query = $this->db->prepare("SELECT Pasajeros.first_name, Pasajeros.last_name, Paquetes.paq, Paquetes.pventa, Paquetes.dif, Paquetes.vendedor FROM Pasajeros INNER JOIN Paquetes ON Paquetes.idPasajero = Pasajeros.id  WHERE Paquetes.fecha BETWEEN :fecha1 AND :fecha2 ORDER by Pasajeros.id desc"); 
        $query->bindParam("fecha1", $fecha1, PDO::PARAM_STR);
        $query->bindParam("fecha2", $fecha2, PDO::PARAM_STR);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }




    public function Details($user_id, $paqID)
    {
        //$query = $this->db->prepare("SELECT * FROM users WHERE id = :id"); 
        //$query = $this->db->prepare("SELECT * FROM ConsultaCompleta WHERE id = :id");
        $query = $this->db->prepare("SELECT Pasajeros.id,Pasajeros.first_name, Pasajeros.last_name,Pasajeros.email,Pasajeros.tel,Pasajeros.fnac,Pasajeros.dni,Paquetes.id AS paqueteID,Paquetes.idPagos, Paquetes.paq, Paquetes.pventa, Paquetes.pcosto, Paquetes.dif, Paquetes.vendedor,Pasajeros.telseg,Pasajeros.familiar,Pagos.opid, Pagos.tipoPago as tipopago FROM Pasajeros INNER JOIN Paquetes ON Paquetes.idPasajero = Pasajeros.id INNER JOIN Pagos ON Paquetes.idPagos = Pagos.id WHERE Pasajeros.id = :id AND Paquetes.id = :paqID ORDER by Pasajeros.id desc"); 
        $query->bindParam("id", $user_id, PDO::PARAM_STR);
        $query->bindParam("paqID", $paqID, PDO::PARAM_STR);
        $query->execute();
        return json_encode($query->fetch(PDO::FETCH_ASSOC));
    }

    public function Buscar($param,$param2)
    {
    $query = $this->db->prepare("SELECT Pasajeros.first_name,Pasajeros.last_name,Pasajeros.email,Pasajeros.fnac,Pasajeros.dni,Pasajeros.tel,Pasajeros.telseg,
Pasajeros.familiar,Paquetes.vendedor,MIN(Paquetes.id)
 FROM Pasajeros INNER JOIN Paquetes ON Paquetes.idPasajero = Pasajeros.id WHERE dni = :param OR last_name LIKE :param2"); 
        $term = '%' . $param2 . '%';
        $query->bindParam("param", $param, PDO::PARAM_STR);
        $query->bindValue("param2", $term, PDO::PARAM_STR);
        $query->execute();
        return json_encode($query->fetch(PDO::FETCH_ASSOC));
    }

    public function Buscar_NombreYApellido($param,$param2)
    {
    $query = $this->db->prepare("SELECT Pasajeros.id,first_name,last_name, email AS correo,tel AS telefono,Paquetes.paq,Paquetes.pventa, Paquetes.id AS paqID FROM Pasajeros INNER JOIN Paquetes ON Pasajeros.id = Paquetes.idPasajero WHERE dni = :param OR last_name LIKE :param2  LIMIT 1"); 
        $term = '%' . $param2 . '%';
        $query->bindParam("param", $param, PDO::PARAM_STR);
        $query->bindValue("param2", $term, PDO::PARAM_STR);
        $query->execute();
        return json_encode($query->fetch(PDO::FETCH_ASSOC));
    }

    public function UltimoPasajero()
    {
    $query = $this->db->prepare("SELECT Pasajeros.id,first_name,last_name, email AS correo,tel AS telefono,Paquetes.paq,Paquetes.pventa, Paquetes.id AS paqID FROM Pasajeros INNER JOIN Paquetes ON Pasajeros.id = Paquetes.idPasajero ORDER BY Paquetes.id DESC  LIMIT 1"); 
        $query->execute();
        return json_encode($query->fetch(PDO::FETCH_ASSOC));
    }

    public function BuscarHistorialPaquetes($param,$param2)
    {
    $query = $this->db->prepare("SELECT * FROM Paquetes WHERE idPasajero = (SELECT id FROM Pasajeros WHERE dni = :param OR last_name LIKE :term LIMIT 1)"); 
        $term = '%' . $param2 . '%';
        $query->bindParam("param", $param, PDO::PARAM_STR);
        $query->bindValue("term", $term, PDO::PARAM_STR);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }



    public function ConsultarComisionTP($tarjeta,$cuotas)
    {
        $query = $this->db->prepare("SELECT * FROM ComisionesTP WHERE cuotas = :cuotas;");
        //$query->bindParam("tarjeta", $tarjeta, PDO::PARAM_STR);
        $query->bindParam("cuotas", $cuotas, PDO::PARAM_INT);
        $query->execute();
        $resultado = $query -> fetch(PDO::FETCH_ASSOC);;
        $comision = $resultado[$tarjeta];
        return $comision;
    }




    public function Comision($idPagos, $comision)
    {

        $query = $this->db->prepare("UPDATE Paquetes SET dif = (pventa-pcosto) - (pventa * :comision) WHERE idPagos = :idPagos"); 
        $query->bindParam("idPagos", $idPagos, PDO::PARAM_STR);   
        $query->bindParam("comision", $comision, PDO::PARAM_INT);
        $query->execute();


    }

   public function Emitir($paqID,$pagoID,$pcosto,$tipopago,$cuotas,$tarjeta)
    {
  
    $query = $this->db->prepare("UPDATE Paquetes SET pcosto = :pcosto WHERE id = :idPaquete");
    $query->bindParam("pcosto", $pcosto, PDO::PARAM_INT);
    $query->bindParam("idPaquete", $paqID, PDO::PARAM_STR);
    $query->execute();

    $query = $this->db->prepare("UPDATE Pagos SET cuotas = :cuotas, tipoTarjeta = :tipotarjeta WHERE id = :pagoID");
    $query->bindParam("cuotas", $cuotas, PDO::PARAM_INT);
    $query->bindParam("tipotarjeta", $tarjeta, PDO::PARAM_STR);
    $query->bindParam("pagoID", $pagoID, PDO::PARAM_STR);
    $query->execute();




    if ($tipopago === 'Efectivo') {

        $query = $this->db->prepare("UPDATE Paquetes SET dif = (pventa-pcosto) WHERE id = :idPaquete"); 
        $query->bindParam("idPaquete", $paqID, PDO::PARAM_STR);   
        $query->execute();
        } else {

            $this->Comision($pagoID, $this->ConsultarComisionTP($tarjeta,$cuotas));

        }



/*
    if ($tarjeta == 'MASTERCARD') {

                            switch ($cuotas) {
                                        case 2:
                                            $this->Comision($pagoID, 0.09);
                                            break;
                                        case 3:
                                            $this->Comision($pagoID, 0.09);
                                            break;
                                        case 4:
                                            $this->Comision($pagoID, 0.09);
                                            break;
                                        case 5:
                                            $this->Comision($pagoID, 0.09);
                                            break;
                                        case 6:
                                            $this->Comision($pagoID, 0.12);
                                            break;
                                        case 7:
                                            $this->Comision($pagoID, 0.12);
                                            break;
                                        case 8:
                                            $this->Comision($pagoID, 0.12);
                                            break;
                                        case 9:
                                            $this->Comision($pagoID, 0.1650);
                                            break;
                                        case 10:
                                            $this->Comision($pagoID, 0.1650);
                                            break;
                                        case 11:
                                            $this->Comision($pagoID, 0.1650);
                                            break;
                                        case 12:
                                            $this->Comision($pagoID, 0.1750);
                                            break;
                                    }

                            
            
    } else { // End Mastercard 

                            
                            switch ($cuotas) {
                                        case 2:
                                            $this->Comision($pagoID, 0.09);
                                            break;
                                        case 3:
                                            $this->Comision($pagoID, 0.09);
                                            break;
                                        case 4:
                                            $this->Comision($pagoID, 0.09);
                                            break;
                                        case 5:
                                            $this->Comision($pagoID, 0.09);
                                            break;
                                        case 6:
                                            $this->Comision($pagoID, 0.11);
                                            break;
                                        case 7:
                                            $this->Comision($pagoID, 0.11);
                                            break;
                                        case 8:
                                            $this->Comision($pagoID, 0.11);
                                            break;
                                        case 9:
                                            $this->Comision($pagoID, 0.15);
                                            break;
                                        case 10:
                                            $this->Comision($pagoID, 0.15);
                                            break;
                                        case 11:
                                            $this->Comision($pagoID, 0.15);
                                            break;
                                        case 12:
                                            $this->Comision($pagoID, 0.17);
                                            break;
                                    }


    } // End Otras

    

 */
    }

    public function DesEmitir($idPaq)
    {
  
    $query = $this->db->prepare("UPDATE Paquetes SET pcosto = 0,dif = 0 WHERE id = :idPaq");
    $query->bindParam("idPaq", $idPaq, PDO::PARAM_STR);
    $query->execute();
    }

     public function PasajerosPaqueteAgencia($eviajes,$paq,$paq2)
    {
         $query = $this->db->prepare("SELECT Pasajeros.id,Pasajeros.first_name, Pasajeros.last_name, Paquetes.paq, Paquetes.pcosto, Paquetes.pventa,Paquetes.id AS paqID, Paquetes.pcosto, Paquetes.dif, Paquetes.vendedor, Paquetes.eviajes FROM Pasajeros INNER JOIN Paquetes ON Paquetes.idPasajero = Pasajeros.id WHERE Paquetes.eviajes = :eviajes AND (Paquetes.paq = :paq OR Paquetes.paq LIKE :paq2) ORDER by paqID desc");
         $term = $paq2 . '%';
         $query->bindParam("eviajes", $eviajes, PDO::PARAM_STR);
         $query->bindParam("paq", $paq, PDO::PARAM_STR);
         $query->bindParam("paq2", $term, PDO::PARAM_STR);
         //$query = $this->db->prepare("SELECT * FROM ConsultaCompleta order by id  LIMIT 10");
        /* $ $query = $this->db->prepare("SELECT Pasajeros.first_name, Pasajeros.last_name, Paquetes.nombre, Paquetes.pventa, Paquetes.pcosto, Paquetes.dif, Paquetes.vendedor FROM Pasajeros INNER JOIN Paquetes ON Pasajeros.idPasajeros = Paquetes.idPaquete ORDER by Pasajeros.idPasajeros desc LIMIT 10"); */
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }




    public function Exportar($paq)
    {

        $query = $this->db->prepare("SELECT * FROM users WHERE paq = :paq");
                $query->bindParam("paq", $paq, PDO::PARAM_STR);
                $query->execute();
                $data = array();
                while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                    $data[] = $row;
                }
                return $data;


         
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=EMT_Pasajeros.csv');
        $output = fopen('php://output', 'w');
        fputcsv($output, array('ID', 'Nombre', 'Apellido', 'Email', 'F.Nacimiento', 'Telefono', 'Tel.Seguro', 'Paquete','$ VENTA', '$ EMISIÓN', '$ NETA', 'DNI', 'Vendedor', 'Link Comprobante'));
         
        if (count($users) > 0) {
            foreach ($users as $row) {
                fputcsv($data, $row);
            }
        }









    } // Fin exportar





}

?>