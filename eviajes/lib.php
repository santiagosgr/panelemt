<?php

require 'db_connection.php';

class CRUD
{

    public $db;

    function __construct()
    {
        $this->db = DB();
    }

    function __destruct()
    {
        $this->db = null;
    }

    /*
     * Add new Record
     *
     * @param $first_name
     * @param $last_name
     * @param $email
     * @return $mixed
     * */


public function MenuPaquetes()
    {
        $query = $this->db->prepare("SELECT * FROM NombrePaquetes WHERE nombre  NOT LIKE '%-%' order by id desc LIMIT 3");
        //$term = '%' . $param2 . '%';
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }



public function BuscarHistorialPaquetes($param,$param2,$eviajes)
    {
    $query = $this->db->prepare("SELECT paq,pventa,vendedor FROM Paquetes WHERE idPasajero = (SELECT id FROM Pasajeros WHERE dni = :param OR last_name LIKE :term LIMIT 1) AND eviajes = :eviajes"); 
        $term = '%' . $param2 . '%';
        $query->bindParam("param", $param, PDO::PARAM_STR);
        $query->bindValue("term", $term, PDO::PARAM_STR);
        $query->bindParam("eviajes", $eviajes, PDO::PARAM_STR);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

public function ConsultaPaquetesDisponibles()
    {
        $query = $this->db->prepare("SELECT nombre,descripcion FROM NombrePaquetes order by id desc LIMIT 3");
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

public function ConsultarTarjetas()
    {
        $query = $this->db->prepare("SELECT nombre FROM Tarjetas order by id asc");
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }



    public function SinEmitir($eviajes)
    {
         $query = $this->db->prepare("SELECT Pasajeros.id,Pasajeros.first_name, Pasajeros.last_name, Paquetes.paq, Paquetes.pcosto, Paquetes.pventa,Paquetes.id AS paqID, Paquetes.dif, Paquetes.vendedor, Paquetes.eviajes FROM Pasajeros INNER JOIN Paquetes ON Paquetes.idPasajero = Pasajeros.id WHERE Paquetes.eviajes = :eviajes AND Paquetes.pcosto = 0 ORDER by paqID desc");
         $query->bindParam("eviajes", $eviajes, PDO::PARAM_STR);
         //$query = $this->db->prepare("SELECT * FROM ConsultaCompleta order by id  LIMIT 10");
        /* $ $query = $this->db->prepare("SELECT Pasajeros.first_name, Pasajeros.last_name, Paquetes.nombre, Paquetes.pventa, Paquetes.pcosto, Paquetes.dif, Paquetes.vendedor FROM Pasajeros INNER JOIN Paquetes ON Pasajeros.idPasajeros = Paquetes.idPaquete ORDER by Pasajeros.idPasajeros desc LIMIT 10"); */
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }
    public function Emitidos($eviajes)
    {
         $query = $this->db->prepare("SELECT Pasajeros.id,Pasajeros.first_name, Pasajeros.last_name, Paquetes.paq, Paquetes.pcosto, Paquetes.pventa,Paquetes.id AS paqID, Paquetes.pcosto, Paquetes.dif, Paquetes.vendedor, Paquetes.eviajes FROM Pasajeros INNER JOIN Paquetes ON Paquetes.idPasajero = Pasajeros.id WHERE Paquetes.eviajes = :eviajes AND Paquetes.pcosto != 0 ORDER by paqID desc LIMIT 15");
         $query->bindParam("eviajes", $eviajes, PDO::PARAM_STR);
         //$query = $this->db->prepare("SELECT * FROM ConsultaCompleta order by id  LIMIT 10");
        /* $ $query = $this->db->prepare("SELECT Pasajeros.first_name, Pasajeros.last_name, Paquetes.nombre, Paquetes.pventa, Paquetes.pcosto, Paquetes.dif, Paquetes.vendedor FROM Pasajeros INNER JOIN Paquetes ON Pasajeros.idPasajeros = Paquetes.idPaquete ORDER by Pasajeros.idPasajeros desc LIMIT 10"); */
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }
     public function PasajerosPaqueteAgencia($eviajes,$paq)
    {
         $query = $this->db->prepare("SELECT Pasajeros.id,Pasajeros.first_name, Pasajeros.last_name, Paquetes.paq, Paquetes.pcosto, Paquetes.pventa,Paquetes.id AS paqID, Paquetes.pcosto, Paquetes.dif, Paquetes.vendedor, Paquetes.eviajes FROM Pasajeros INNER JOIN Paquetes ON Paquetes.idPasajero = Pasajeros.id WHERE Paquetes.eviajes = :eviajes AND Paquetes.paq = :paq ORDER by paqID desc");
         $query->bindParam("eviajes", $eviajes, PDO::PARAM_STR);
         $query->bindParam("paq", $paq, PDO::PARAM_STR);
         //$query = $this->db->prepare("SELECT * FROM ConsultaCompleta order by id  LIMIT 10");
        /* $ $query = $this->db->prepare("SELECT Pasajeros.first_name, Pasajeros.last_name, Paquetes.nombre, Paquetes.pventa, Paquetes.pcosto, Paquetes.dif, Paquetes.vendedor FROM Pasajeros INNER JOIN Paquetes ON Pasajeros.idPasajeros = Paquetes.idPaquete ORDER by Pasajeros.idPasajeros desc LIMIT 10"); */
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    public function PasajerosPaquete($paq)
    {
        $query = $this->db->prepare("SELECT * FROM users WHERE paq = :paq");
        $query->bindParam("paq", $paq, PDO::PARAM_STR);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }




public function Rentabilidad()
    {
        $query = $this->db->prepare("SELECT SUM(dif) AS sum_dif FROM users WHERE pcosto != 0");
        $query->execute();
        $resultado = $query -> fetch(PDO::FETCH_ASSOC);
        $total = $resultado['sum_dif'];
        return $total;
    }

    public function RentabilidadPaquete($paq)
    {
        $query = $this->db->prepare("SELECT SUM(dif) AS sum_dif FROM users WHERE paq = :paq AND pcosto != 0");
        $query->bindParam("paq", $paq, PDO::PARAM_STR);
        $query->execute();
        $resultado = $query -> fetch(PDO::FETCH_ASSOC);
        $total = $resultado['sum_dif'];
        return $total;
    }




    public function PasajerosEmitidosPorAgencia($eviajes)
    {
        $query = $this->db->prepare("SELECT * FROM users WHERE eviajes = :eviajes AND pcosto != 0");
        $query->bindParam("eviajes", $eviajes, PDO::PARAM_STR);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

     public function CantidadSinEmitir()
    {
        $query = $this->db->prepare("SELECT id FROM Paquetes WHERE pcosto = 0");
        $query->execute();
        $cantidad = $query->rowCount();
        return $cantidad;
    }

    public function EmitidosPorPaquete($paq)
    {
        $query = $this->db->prepare("SELECT id FROM Paquetes WHERE paq = :paq AND pcosto != 0");
        $query->bindParam("paq", $paq, PDO::PARAM_STR);
        $query->execute();
        $cantidad = $query->rowCount();
        return $cantidad;
    }



    /*
     * Delete Record
     *
     * @param $user_id
     * */
    public function Delete($user_id)
    {
        $query = $this->db->prepare("DELETE FROM users WHERE id = :id");
        $query->bindParam("id", $user_id, PDO::PARAM_STR);
        $query->execute();
    }

    public function EliminarPaquetes($paq)
    {
        $query = $this->db->prepare("DELETE FROM users WHERE paq = :paq");
        $query->bindParam("paq", $paq, PDO::PARAM_STR);
        $query->execute();
    }


      public function TipoPago($user_id)
    {
        $query = $this->db->prepare("SELECT tipopago FROM users WHERE id = :id");
        $query->bindParam("id", $user_id, PDO::PARAM_STR);
        $query->execute();
        $row = $query->fetch(PDO::FETCH_ASSOC);
        return $row['tipopago'];
    }

    public function Comision($id, $comision)
    {

        $query = $this->db->prepare("UPDATE users SET dif = (pventa-pcosto) - (pventa * :comision) WHERE id = :id"); 
        $query->bindParam("id", $id, PDO::PARAM_STR);   
        $query->bindParam("comision", $comision, PDO::PARAM_INT);
        $query->execute();


    }



    public function Update($pcosto, $id, $tipopago,$cuotas, $tarjeta)
    {
  
    $query = $this->db->prepare("UPDATE users SET pcosto = :pcosto WHERE id = :id");
    $query->bindParam("pcosto", $pcosto, PDO::PARAM_INT);
    $query->bindParam("id", $id, PDO::PARAM_STR);
    $query->execute();

    $query = $this->db->prepare("UPDATE users SET cuotas = :cuotas, tipotarjeta = :tipotarjeta WHERE id = :id");
    $query->bindParam("cuotas", $cuotas, PDO::PARAM_INT);
    $query->bindParam("tipotarjeta", $tarjeta, PDO::PARAM_STR);
    $query->bindParam("id", $id, PDO::PARAM_STR);
    $query->execute();



    if ($tipopago === 'Efectivo') {

        $query = $this->db->prepare("UPDATE users SET dif = (pventa-pcosto) WHERE id = :id"); 
        $query->bindParam("id", $id, PDO::PARAM_STR);   
        $query->execute();
        } 
	else {
			if ($tipopago === 'MASTERCARD') {
					
                            switch ($cuotas) {
                                        case 2:
                                            $this->Comision($id, 0.09);
                                            break;
                                        case 3:
                                            $this->Comision($id, 0.09);
                                            break;
                                        case 4:
                                            $this->Comision($id, 0.09);
                                            break;
                                        case 5:
                                            $this->Comision($id, 0.09);
                                            break;
                                        case 6:
                                            $this->Comision($id, 0.12);
                                            break;
                                        case 7:
                                            $this->Comision($id, 0.12);
                                            break;
                                        case 8:
                                            $this->Comision($id, 0.12);
                                            break;
                                        case 9:
                                            $this->Comision($id, 0.1650);
                                            break;
                                        case 10:
                                            $this->Comision($id, 0.1650);
                                            break;
                                        case 11:
                                            $this->Comision($id, 0.1650);
                                            break;
                                        case 12:
                                            $this->Comision($id, 0.1750);
                                            break;
                                    }

            } else {		switch ($cuotas) {
                                        case 2:
                                            $this->Comision($id, 0.09);
                                            break;
                                        case 3:
                                            $this->Comision($id, 0.09);
                                            break;
                                        case 4:
                                            $this->Comision($id, 0.09);
                                            break;
                                        case 5:
                                            $this->Comision($id, 0.09);
                                            break;
                                        case 6:
                                            $this->Comision($id, 0.11);
                                            break;
                                        case 7:
                                            $this->Comision($id, 0.11);
                                            break;
                                        case 8:
                                            $this->Comision($id, 0.11);
                                            break;
                                        case 9:
                                            $this->Comision($id, 0.15);
                                            break;
                                        case 10:
                                            $this->Comision($id, 0.15);
                                            break;
                                        case 11:
                                            $this->Comision($id, 0.15);
                                            break;
                                        case 12:
                                            $this->Comision($id, 0.17);
                                            break;
                                    }






            }

                            
            
    }

    

 
    }

    /*
     * Get Details
     *
     * @param $user_id
     
    public function Details($user_id)
    {
        //$query = $this->db->prepare("SELECT * FROM users WHERE id = :id"); 
        //$query = $this->db->prepare("SELECT * FROM ConsultaCompleta WHERE id = :id");
        $query = $this->db->prepare("SELECT Pasajeros.id,Pasajeros.first_name, Pasajeros.last_name,Pasajeros.email,Pasajeros.tel,Pasajeros.fnac,Pasajeros.dni,Paquetes.id AS paqueteID,Paquetes.idPagos, Paquetes.paq, Paquetes.pventa, Paquetes.pcosto, Paquetes.dif, Paquetes.vendedor,Pasajeros.telseg,Pasajeros.familiar, Pagos.tipoPago as tipopago FROM Pasajeros INNER JOIN Paquetes ON Paquetes.idPasajero = Pasajeros.id INNER JOIN Pagos ON Paquetes.idPagos = Pagos.id WHERE Pasajeros.id = :id ORDER by Pasajeros.id desc"); 
        $query->bindParam("id", $user_id, PDO::PARAM_STR);
        $query->execute();
        return json_encode($query->fetch(PDO::FETCH_ASSOC));
    }

* */

  



}

?>