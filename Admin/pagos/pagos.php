<?php


require '../pasajeros/ajax/lib.php';
    session_start();

    if(isset($_SESSION['usuario'])){

        if($_SESSION['Tipo_usuario'] == "Admin"){
            

?>



<!DOCTYPE html>
<html>
<!DOCTYPE html>
<html lang="en">
  <head>

  
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../EMT.ico">

    <title>Panel EMT</title>


 <!-- Jquery JS file -->
<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
 
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.5.16/clipboard.min.js"></script>

 
<!-- Custom JS file -->
<script type="text/javascript" src="js/script.js"></script>
<!-- <script type="text/javascript" src="../js/main.js"></script> -->







    <!-- Custom styles for this template -->




  </head>

  <body>
  <script>
$(document).ready(function () {
    // READ records on page load
    readRecords('EmitidosConTarjeta'); // calling function  
    $("#nodata").hide();
    $("#data").hide();
    $('#paxnotfound').hide();
    $('#maxCuotas').hide(); $('#labelMaxCuotas').hide(); $('#generarLink').hide();





$("#cerrarinfo").click( function() {

  $("#data").hide();
  $("#nodata").hide();
  $("#estado").css('color', 'initial');
  $("#estado").css("font-weight","initial");



    $('[data-toggle="tooltip"]').tooltip(); 




});





});



</script>



<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Electronic Music Travel</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="../index.php">Estadísticas</a></li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Pasajeros <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="../pasajeros/index.php">Últimos pasajeros</a></li>
            <li class="divider"></li>

            <?php 
                                      $info = new CRUD();
                                      $paquetes = $info->MenuPaquetes();


                                      if (count($paquetes) > 0) {
                                              foreach ($paquetes as $paquete) {
                                                    echo '<li><a href=../pasajeros/' . strtolower($paquete['nombre']) . '.php>' . $paquete['descripcion'] . '</a></li>';
                                                                              }
                                                                }
                                      


                      ?>

          </ul>
        </li>
        <li class="active"><a href="pagos.php">Pagos</a></li>
        <li><a href="../historico/index.php">Histórico</a></li>
        <li><a href="../paquetes/paquetes.php">Paquetes</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="../perfil/index.php"><span class="glyphicon glyphicon-user"></span><?php echo $_SESSION['Nombre']; echo '&nbsp'; echo $_SESSION['Apellido'];  ?></a></li>
        <li><a href="../../logout.php"><span class="glyphicon glyphicon-log-in"></span> Cerrar sesión</a></li>
      </ul>
    </div>
  </div>
</nav>








<div class="container-fluid">

<!--
            <div class="pull-left">
                <button class="btn btn-success" data-toggle="modal" data-target="#add_new_record_modal">Añadir pasajero</button>
            </div>
            -->


               

    <div class="row">
        <div class="col-sm-12 col-sm-offset- col-md-12-offset-2 main">
        



<div class="panel panel-default">
        <div class="panel-heading text-center"><h3>Consultas pagos - Todo Pago</h3><br>


        	<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#linkPersonalizado">
  Generar link personalizado
</button>
        </div>    
        <!--<h3>Últimos pasajeros cargados</h3>  -->




<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="linkPersonalizado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Generar link personalizado</h4>
      </div>
      <div class="modal-body">




        <div class="form-group has-success">
                    <div class="input-group input-group-lg">
                      <div class="input-group-addon">Buscar</div>
                      <input type="text" class="search-query form-control" placeholder="Ingregar DNI o apellido del pasajero" id="dnibuscar">
                      
                      <span class="input-group-btn">
                                    <button class="btn btn-info"   id="btnultimo" data-toggle="tooltip" data-placement="top" title="Último pasajero agregado" onclick="UltimoPasajero()">
                                        <span class="glyphicon glyphicon-hourglass"></span>
                                    </button>
                                    <button class="btn btn-success" onclick="Buscar()" id="btnbuscar">
                                        <span class=" glyphicon glyphicon-search"></span>
                                    </button>
                                </span>
                      
                    </div>
                  </div>
                  <input type="hidden" id="nombre">
                  <input type="hidden" id="ap">
                  <input type="hidden" id="dni">
                  <input type="hidden" id="paqID">
                   <input type="hidden" id="paq">
                  <input type="hidden" id="userID">
                  <input type="hidden" id="pventa">
                  <input type="hidden" id="tel">
                  <input type="hidden" id="email">
                  <p class="text-danger" id="paxnotfound"><b>Error!</b>Usuario no encontrado</p>
                  <p class="text-success" id="usuarioEncontrado"><b></b></p>
                  


               <div id="divcuotas">
                 <label id="labelMaxCuotas" for="cuotas">Máximo de cuotas</label>
              <select id="maxCuotas" class="selectpicker">
              <optgroup label="Seleccionar cantidad MÁXIMA de cuotas para el pago">
              	<option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
              <option>6</option>
              <option>7</option>
              <option>8</option>
              <option>9</option>
              <option>10</option>
              <option>11</option>
              <option>12</option>

              </optgroup>
            </select>
            <button type="button" class="btn btn-info" id="generarLink" onclick="GenerarLink()">Generar link</button>
        </div>

             <script>
              var clipboard = new Clipboard('#copiar');

                clipboard.on('success', function(e) {
                    e.clearSelection();
                    //alert('Copy to Clipboard Successfully');
                });

                clipboard.on('error', function(e) {
                    //alert('Something is wrong!');
                });

            </script>


            <div class="input-group input-group-sm" id="prueba2">
                  <span class="input-group-addon" id="basic-addon1"><button type="button" data-clipboard-action="copy" class="btn btn-info btn-xs glyphicon glyphicon-file" id="copiar" data-clipboard-target="#linkmp" style="display: none"></button></span>
                    <input type="text" class="form-control" id="linkmp" style="display: none" readonly>
                </div>


                  <script type="text/javascript">

                  	$('#dnibuscar').keypress(function (e) {
						 var key = e.which;
						 if(key == 13)  // the enter key code
						  {
						    $('#btnbuscar').click();
						    return false;  
						  }
						});
                  </script>

                 


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	
$('#linkPersonalizado').on('show.bs.modal', function (e) {
 $("#prueba2").hide();
});

$('#linkPersonalizado').on('hidden.bs.modal', function (e) {
  $("#usuarioEncontrado").hide();
  $('#divcuotas').hide();
})


</script>









        <div class="table-responsive">
          <div class="records_content"></div> 
        </div>

</div>

</div>



</div>
</div>


<!-- Modal -->
<div class="modal fade" id="datosPago" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Datos de la transacción</h4>
      </div>
      <div class="modal-body">

<div class="alert alert-danger" id="nodata" role="alert"><b>No hay información!</b> El pasajero aún no pagó o no se registro el pago. </div>
<div class="alert alert-success" id="data" role="alert"><b> Se registró la transacción correctamente.</b></div>
    
        Titular tarjeta de crédito:<p id="nomtarjeta"></p>
        Estado de transacción:<p id="estado"></p>
        Fecha transacción:<p id="fecha"></p>
        Tarjeta de crédito:<p id="tarjeta"></p>
        Número de tarjeta de crédito:<p id="numtarjeta"></p>
        $ pagada por pasajero:<p id="precio"></p>
        $ acreditados en cuenta TP:<p id="trip"></p>
        Cuotas:<p id="cuotas"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-danger" id="cerrarinfo" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>







  </body>
</html>

<?php

}else {
    
    header("Location: ../../");
}

} else {
    header("Location: ../../");
}
?>